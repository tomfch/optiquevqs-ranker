package eu.optiquevqs.ranker.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.graph.query.QueryVariable;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.SimpleQuery;
import eu.optiquevqs.ranker.data_structures.TestQuery;
import eu.optiquevqs.ranker.enums.Category;
import eu.optiquevqs.ranker.rankers.FactorizationRanker;
import eu.optiquevqs.ranker.rankers.ItemRanker;
import eu.optiquevqs.ranker.rankers.Model;
import eu.optiquevqs.ranker.rankers.MostPopularRanker;
import eu.optiquevqs.ranker.rankers.RandomRanker;
import eu.optiquevqs.ranker.rankers.Ranker;
import eu.optiquevqs.ranker.rankers.RulesRanker;

public class EvaluationUtils {
	// Partitions querylog into k equal weigthed partitions.
	public static List<QueryLog> crossValidation(QueryLog querylog, int k) {
		List<QueryLog> partitions = new ArrayList<>(k);
		for(int i = 0; i < k; i++) partitions.add(new QueryLog(querylog.getNavigationGraph()));
		Iterator<QueryGraph> it = querylog.iterator();
		while(it.hasNext()) {
			QueryGraph currentQuery = it.next();
			distributeQuery(partitions, currentQuery);
		}
		return partitions;
	}

	// Split query log into 2 partitions. 0 < splitRatio < 1.
	public static List<QueryLog> singleSplit(QueryLog querylog, double splitRatio) {
		List<QueryLog> partitions = new ArrayList<>(2);
		for(int i = 0; i < 2; i++) partitions.add(new QueryLog(querylog.getNavigationGraph()));
		Iterator<QueryGraph> it = querylog.iterator();
		while(it.hasNext()) {
			QueryGraph currentQuery = it.next();
			distributeQuery(partitions, currentQuery, splitRatio);
		}
		return partitions;
	}

	// Distribute a query to two querylog given a split ratio.
	private static void distributeQuery(List<QueryLog> partitions, QueryGraph query, double splitRatio) {
		double weight = query.getWeight();
		double[] weightDistribution = sample(splitRatio, weight);
		for (int i = 0; i < 2; i++) {
			if (weightDistribution[i] > 0.0) {
				QueryGraph newQuery = new QueryGraph(query);
				newQuery.setWeight(weightDistribution[i]);
				partitions.get(i).addQueryGraph(newQuery);
			}
		}
	}

	// Distribute a query to n number of partitions equal weighted partitions.
	private static void distributeQuery(List<QueryLog> partitions, QueryGraph query) {
		int partitionsCount = partitions.size();
		double weight = query.getWeight();
		double[] weightDistribution = sample(partitionsCount, weight);
		for (int i = 0; i < partitionsCount; i++) {
			if (weightDistribution[i] > 0.0) {
				QueryGraph newQuery = new QueryGraph(query);
				newQuery.setWeight(weightDistribution[i]);
				partitions.get(i).addQueryGraph(newQuery);
			}
		}
	}

	// Sample weights between two partitions with a split ratio.
	private static double[] sample(double splitRatio, double weight) {
		double[] weightDistribution = new double[2];
		double remainingWeight = weight;
		while(remainingWeight >= 1.0) {
			if (Math.random() > splitRatio) {
				weightDistribution[0] += 1.0;
			} else {
				weightDistribution[1] += 1.0;
			}
			remainingWeight -= 1.0;
		}
		if(remainingWeight > 0.0) {
			if(Math.random() > splitRatio) {
				weightDistribution[0] += remainingWeight;
			} else {
				weightDistribution[1] += remainingWeight;
			}
		}
		return weightDistribution;
	}

	// Sample weights equally between n number of partitions
	private static double[] sample(int partitionsCount, double weight) {
		double[] weightDistribution = new double[partitionsCount];
		double remainingWeight = weight;
		while(remainingWeight >= 1.0) {
			int index = (int) (Math.random() * partitionsCount);
			weightDistribution[index] += 1.0;
			remainingWeight -= 1.0;
		}
		if(remainingWeight > 0.0) {
			int index = (int) (Math.random() * partitionsCount);
			weightDistribution[index] += remainingWeight;
		}
		return weightDistribution;
	}

	public static File copyTempFileToPath(File tempFile, String path) {
		File file = new File(path);
		Scanner scanner = null;
		try {
			scanner = new Scanner(tempFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		PrintWriter printWriter = new PrintWriter(fileWriter);
		while(scanner.hasNextLine()) {
			printWriter.println(scanner.nextLine());
		}
		scanner.close();
		printWriter.close();
		return file;
	}

	public static <T> T getRandomElement(Set<T> set) {
		int randomIndex = new Random().nextInt(set.size());
		int i = 0;
		for(T t : set) {
			if(i == randomIndex) return t;
			i++;
		}
		throw new Error("random index not found");
	}

	// Avoid infinite loop for small query logs where splitRatio is not divisible by querylog size.
	public static void infiniteLoopCheck(int i) {
		String infiniteLoopWarning = "Infinite loop for test set sampling. Try another splitRatio for evaluation.";
		if(i > 100) throw new IllegalStateException(infiniteLoopWarning);
	}

	public static Model trainModel(QueryLog querylog, RankerConfig rankerConfig) {
		Ranker simpleRanker = null;
		switch(rankerConfig.rankerType) {
			case ITEM :
				simpleRanker = new ItemRanker(querylog, rankerConfig);
				break;
			case MOSTPOPULAR :
				simpleRanker = new MostPopularRanker(querylog, rankerConfig);
				break;
			case FACTORIZATION :
				simpleRanker = new FactorizationRanker(querylog, rankerConfig);
				break;
			case RANDOM :
				simpleRanker = new RandomRanker(querylog, rankerConfig);
				break;
			case RULES :
				simpleRanker = new RulesRanker(querylog, rankerConfig);
				break;
			default:
				throw new IllegalArgumentException(rankerConfig.rankerType + " is not a valid ranker type.");
		}
		return simpleRanker.createModel();
	}

	/* 
	 * Choose random edge. Chose random node connected to chosen edge with the following restrictions:
	 * If one of the connected nodes is a leaf node, this node will be cut.
	 */
	public static TestQuery createTestQuery(QueryGraph query) {
		QueryGraph queryCopy = new QueryGraph(query);
		Set<PropertyEdge> edges = queryCopy.edgeSet();

		// Choose random edge
		PropertyEdge cutEdge = EvaluationUtils.getRandomElement(edges);
		QueryVariable edgeSource = queryCopy.getEdgeSource(cutEdge);
		QueryVariable edgeTarget = queryCopy.getEdgeTarget(cutEdge);
		boolean cutEdgeIsObjectProperty = queryCopy.getNavigationGraph().isObjectProperty(cutEdge.getLabel());

		// Choose random node (source or target) connected to chosen edge
		boolean cutTarget = new Random().nextBoolean();
		if(queryCopy.isLeaf(queryCopy.getEdgeTarget(cutEdge))) cutTarget = true;
		if(queryCopy.isLeaf(queryCopy.getEdgeSource(cutEdge))) cutTarget = false;

		String focusNodeType = cutTarget ? edgeSource.getType() : edgeTarget.getType();
		String cutNodeType = cutTarget ? edgeTarget.getType() : edgeSource.getType();
		Category category = cutEdgeIsObjectProperty ? Category.OBJECT : Category.DATATYPE;
		String correctProperty = cutEdge.getLabel();

		queryCopy.removeSubGraph(cutEdge, cutTarget ? edgeTarget : edgeSource);
		return new TestQuery(queryCopy, focusNodeType, category, correctProperty, cutNodeType, !cutTarget);
	}

	// Possible to be left with a query that consist of only a single variable.
	public static TestQuery createTestQuery2(QueryGraph query) {
		QueryGraph queryCopy = new QueryGraph(query);
		Set<PropertyEdge> edges = queryCopy.edgeSet();

		// Choose random edge
		PropertyEdge cutEdge = EvaluationUtils.getRandomElement(edges);
		QueryVariable edgeSource = queryCopy.getEdgeSource(cutEdge);
		QueryVariable edgeTarget = queryCopy.getEdgeTarget(cutEdge);
		boolean cutEdgeIsObjectProperty = queryCopy.getNavigationGraph().isObjectProperty(cutEdge.getLabel());

		boolean cutTarget;
		if(!cutEdgeIsObjectProperty) {
			cutTarget = true;
		} else {
			cutTarget = new Random().nextBoolean();
		}

		String focusNodeType = cutTarget ? edgeSource.getType() : edgeTarget.getType();
		String cutNodeType = cutTarget ? edgeTarget.getType() : edgeSource.getType();
		Category category = cutEdgeIsObjectProperty ? Category.OBJECT : Category.DATATYPE;
		String correctProperty = cutEdge.getLabel();

		queryCopy.removeSubGraph(cutEdge, cutTarget ? edgeTarget : edgeSource);
		return new TestQuery(queryCopy, focusNodeType, category, correctProperty, cutNodeType, !cutTarget);

	}


	public static String top3(List<Extension> extensions) {
		String top = "";
		int i = 0;
		for(Extension ext : extensions) {
			if(i > 2) break;
			top += ext + ", ";
		}
		return top.substring(0, top.length() - 2);
	}

	/*
	 *  Considers two edges with the same label to be the same edge.
	 *  A query with 75% of edges being the same edge return true. Otherwise false.
	 */
	public static boolean queryIsSameEdgeQuery(QueryGraph query) {
		Set<PropertyEdge> edges = query.edgeSet();
		Map<String, Integer> edgeCounts = new HashMap<>();
		for(PropertyEdge edge : edges) {
			String edgeLabel = edge.getLabel();
			int count = edgeCounts.containsKey(edgeLabel) ? edgeCounts.get(edgeLabel) : 0;
			edgeCounts.put(edgeLabel, count + 1);
		}
		for(Integer edgeCount : edgeCounts.values()) {
			if((double)edgeCount / edges.size() >= .75) return true;
		}
		return false;
	}

}
