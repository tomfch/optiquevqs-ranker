package eu.optiquevqs.ranker.utils;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.jgrapht.traverse.DepthFirstIterator;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.graph.query.QueryVariable;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.Item;
import eu.optiquevqs.ranker.data_structures.ItemClass;
import eu.optiquevqs.ranker.data_structures.ItemProperty;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RatingsMatrix;
import eu.optiquevqs.ranker.data_structures.SimpleQuery;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.data_structures.Triple;
import eu.optiquevqs.ranker.enums.Category;

public class RankerUtils {
	public static SimpleQueryLog queryLogToSimpleQueryLog(QueryLog querylog) {
		SimpleQueryLog simpleLog = new SimpleQueryLog(querylog.getNavigationGraph());

		Iterator<QueryGraph> it = querylog.iterator();
		while (it.hasNext()) {
			simpleLog.add(queryGraphToSimpleQuery(it.next()));
		}
		simpleLog.prepareGlobalStats();
		return simpleLog;
	}

	public static SimpleQuery queryGraphToSimpleQuery(QueryGraph queryGraph) {
		SimpleQuery sq = new SimpleQuery();
		DepthFirstIterator<QueryVariable, PropertyEdge> it = new DepthFirstIterator<QueryVariable, PropertyEdge>(queryGraph);
		while (it.hasNext()) {
			QueryVariable currentNode = it.next();
			Set<PropertyEdge> outgoingEdges = queryGraph.outgoingEdgesOf(currentNode);
			for (PropertyEdge edge : outgoingEdges) {
				QueryVariable target = queryGraph.getEdgeTarget(edge);
				String s = currentNode.getType();
				String e = edge.getLabel();
				String t = target.getType();
				Category c = queryGraph.isObjectVariable(target) ? Category.OBJECT : Category.DATATYPE;
				sq.addTriple(s, e, t, c);
			}
		}
		sq.setWeight(queryGraph.getWeight());
		return sq;
	}

    // https://stackoverflow.com/questions/8422374/java-multi-dimensional-array-transposing
    public static double[][] transpose (double[][] array) {
    	if (array == null || array.length == 0) return array;

    	int width = array.length;
    	int height = array[0].length;

    	double[][] array_new = new double[height][width];

    	for (int x = 0; x < width; x++) {
    		for (int y = 0; y < height; y++) {
    			array_new[y][x] = array[x][y];
    		}
    	}
    	return array_new;
    }

    // Dice index (aka Sorensen index) with weights.
    public static double diceSimilarityModified(double[] vector1, double[] vector2, double[] weights) {
    	verifyEqualVectorLengths(vector1, vector2, weights);
    	double a = 0.0;
		double b = 0.0;
		double c = 0.0;
		for(int i = 0; i < weights.length; i++) {
			a += weights[i] * vector1[i] * vector2[i];
			b += weights[i] * vector1[i] * vector1[i];
			c += weights[i] * vector2[i] * vector2[i];
		}
		double result = (2 * a) / (b + c);
		if(Double.isNaN(result)) return 0.0;
		return result;
    }

	// Cos-similarity with weights.
	public static double cosSimilarityModified(double[] vector1, double[] vector2, double[] weights) {
		verifyEqualVectorLengths(vector1, vector2, weights);
		double a = 0.0;
		double b = 0.0;
		double c = 0.0;
		for(int i = 0; i < weights.length; i++) {
			a += weights[i] * vector1[i] * vector2[i];
			b += weights[i] * vector1[i] * vector1[i];
			c += weights[i] * vector2[i] * vector2[i];
		}
		double result =  a / (Math.sqrt(b) * Math.sqrt(c));
		if (Double.isNaN(result)) return 0.0;
		return result;
	}

	// Euclidian distance with weights.
	public static double euclidianDistanceModified(double[] vector1, double[] vector2, double[] weights) {
		verifyEqualVectorLengths(vector1, vector2, weights);
		double distance_squared = 0.0;
		for(int i = 0; i < weights.length; i++) {
			distance_squared += weights[i] * Math.pow(vector2[i] - vector1[i], 2);
		}
		double result = Math.sqrt(distance_squared);
		if(Double.isNaN(result)) return 0.0;
		return result;
	}

	// Jaccard index with weights. Self constructed similarity function.
	public static double jaccardIndexOldModified(double[] vector1, double[] vector2, double[] weights) {
		verifyEqualVectorLengths(vector1, vector2, weights);
		double intersection = 0.0;
		double union = 0.0;
		for(int i = 0; i < weights.length; i++) {
			intersection += weights[i] * vector1[i] * vector2[i];
			union += weights[i] * Math.max(vector1[i], vector2[i]);
		}
		double result = intersection / union;
		if(Double.isNaN(result)) return 0.0;
		return result;
	}

	// Ruzicka similarity with weights.
	public static double ruzickaModified(double[] vector1, double[] vector2, double[] weights) {
		double intersection = 0.0;
		double union = 0.0;
		for(int i = 0; i < weights.length; i++) {
			intersection += weights[i] * Math.min(vector1[i], vector2[i]);
			union += weights[i] * Math.max(vector1[i], vector2[i]);
		}
		double result = intersection / union;
		if(Double.isNaN(result)) return 0.0;
		return result;
	}

	// Jaccard index with weights.
	public static double jaccardIndexModified(double[] vector1, double[] vector2, double[] weights) {
		verifyEqualVectorLengths(vector1, vector2, weights);
		double ab = 0.0;
		double aSquared = 0.0;
		double bSquared = 0.0;
		for(int i = 0; i < weights.length; i++) {
			ab += weights[i] * vector1[i] * vector2[i];
			aSquared += weights[i] * vector1[i] * vector1[i];
			bSquared += weights[i] * vector2[i] * vector2[i];
		}
		double result = ab / (aSquared + bSquared - ab);
		if(Double.isNaN(result)) return 0.0;
		return result;
	}

	private static void verifyEqualVectorLengths(double[] vector1, double[] vector2, double[] weights) {
		if (vector1.length != vector2.length || vector1.length != weights.length) {
			throw new IllegalArgumentException("Vectors must have the same number of elements.");
		}
	}

	// Return 0 if input is 0, else return 1
	public static double zeroOrOne(double number) {
		if(number == 0.0) return 0.0;
		return 1.0;
	}

	public static Set<Item> simpleQueryToItemSet(SimpleQuery simpleQuery, RatingsMatrix matrix) {
		Set<Item> items = new HashSet<>();

		Set<Triple> triples = simpleQuery.getQuery();
		for(Triple t : triples) {
			Item tmpItem = new ItemProperty(t);

			// Triple match a triple in matrix
			if(matrix.getInverseItemMap().containsKey(tmpItem)) {
				items.add(tmpItem);
				continue;
			} 

			// Superclass lookup if triple does not match a triple in matrix
			Set<Triple> superTriples = superTriples(t, matrix.getNavigationGraph());
			for(Triple superTriple : superTriples) {
				tmpItem = new ItemProperty(superTriple);
				if(!matrix.getInverseItemMap().containsKey(tmpItem)) continue;
				items.add(tmpItem);
				break;
			}
		}

		if(matrix.getItemPropertyOnly()) return items;

		Set<String> classes = simpleQuery.getClasses();
		for(String c : classes) items.add(new ItemClass(c));

		return items;
	}

	// Creates all possible triples where subject and object is substitued by its superclasses.
	public static Set<Triple> superTriples(Triple triple, NavigationGraph navGraph) {
		Set<Triple> superTriples = new HashSet<>();
		Set<String> superSubjects = navGraph.getSuperClasses(triple.getSubject());
		Set<String> superObjects = navGraph.getSuperClasses(triple.getObject());
		for(String subject : superSubjects) {
			for(String object : superObjects) {
				superTriples.add(new Triple(subject, triple.getPredicate(), object, triple.getCategory()));
			}
		}
		return superTriples;
	}

	public static Set<Integer> extensionItemIds(Extension extension, RatingsMatrix matrix) {
		Set<Integer> itemIds = new HashSet<>();
		Triple topTriple = extension.getTriple().getTopTriple();
		String subject = extension.getLeft();
		String object = extension.getRight();
		itemIds.add(matrix.getIdFromTriple(topTriple));
		itemIds.add(matrix.getIdFromClass(subject));
		if(extension.getCategory() == Category.OBJECT) {
			itemIds.add(matrix.getIdFromClass(object));
		}
		return itemIds;
	}

	public static boolean itemClassPartOfTriple(Item item, Triple triple) {
		if(!(item instanceof ItemClass)) return false;
		String itemLabel = item.getLabel();
		if(itemLabel.equals(triple.getSubject())) return true;
		if(itemLabel.equals(triple.getObject())) return true;
		return false;
	}
}
