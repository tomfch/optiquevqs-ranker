package eu.optiquevqs.ranker.rankers;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.svd.ALSWRFactorizer;
import org.apache.mahout.cf.taste.impl.recommender.svd.SVDRecommender;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;

import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.ranker.data_structures.RatingsMatrix;
import eu.optiquevqs.ranker.enums.Category;
import eu.optiquevqs.ranker.data_structures.Item;

// NOT IN USE. Not updated according to ranker and model class hierarchy.
// Only for test purposes. Does not support real time recommendations.
public class FactorizationModel {
	private RatingsMatrix matrix;

	public FactorizationModel(RatingsMatrix matrix) {
		this.matrix = matrix;
	}

	public Map<Item, Double> getAllExtensions(QueryGraph query, String focusType, Category category) {
		if (!query.containsClass(focusType)) {
			throw new IllegalArgumentException(focusType + " is not a class in the query.");
		}

		Map<Item, Double> rankedExtensions = new LinkedHashMap<>();

		NavigationGraph ng = query.getNavigationGraph();
		Set<Pair<String, String>> extensions = null;
		switch(category) {
			case DATATYPE :
				extensions = ng.getOutgoingDatatypePairs(focusType);
				break;
			case OBJECT :
				extensions = ng.getOutgoingObjectPairs(focusType);
				break;
			default :
				throw new IllegalArgumentException(category + " is not a valid category. Use \"object\" or \"datatype\"");
		}

		// Training
		int numberOfItems = this.matrix.getMatrix()[0].length;
		//int userId = this.matrix.addUser(query);
		int userId = -1; // DUMMY
		File ratingsFile= this.matrix.createRatingsFile();

		DataModel dataModel = null;
		try {
			dataModel = new FileDataModel(ratingsFile); // file might need to be trimmed to only two columns.
		} catch (IOException e) {
			e.printStackTrace();
		}
		ALSWRFactorizer factorizer = null;
		try {
			factorizer = new ALSWRFactorizer(dataModel, 4, 0.1, 10);
		} catch (TasteException e) {
			e.printStackTrace();
		}
		SVDRecommender recommender = null;
		try {
			recommender = new SVDRecommender(dataModel, factorizer);
		} catch (TasteException e) {
			e.printStackTrace();
		}
		List<RecommendedItem> recs = null;
		try {
			recs = recommender.recommend(userId, numberOfItems);
		} catch (TasteException e) {
			e.printStackTrace();
		}

		// Results
		for(Pair<String, String> outPair : extensions) {
			//rankedExtensions.put(new Item(focusType, outPair.getLeft(), outPair.getRight()), 0.0);
			break; // DUMMY
		}

		for (RecommendedItem ri : recs) {
			Set<Item> items = this.matrix.getItemsFromId((int)ri.getItemID());
			for(Item item : items) {
				if(rankedExtensions.containsKey(item)) rankedExtensions.put(item, (double)ri.getValue());
			}
		}

		//return RankerUtils.sortByValue(rankedExtensions, true);
		return null;

	}
}
