package eu.optiquevqs.ranker.rankers;

import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.neighborhood.Neighborhood;
import eu.optiquevqs.ranker.data_structures.Extensions;

public class ItemRanker extends Ranker {

	public ItemRanker(SimpleQueryLog simpleQuerylog, RankerConfig config) {
		super(simpleQuerylog, config);
	}

	public ItemRanker(QueryLog querylog, RankerConfig config) {
		super(querylog, config);
	}

	@Override
	public Model createModel() {
		Neighborhood neighborhood = new Neighborhood(this.simpleQuerylog, this.config);
		Extensions extensions = this.simpleQuerylog.getExtensions();
		return new ItemModel(neighborhood, extensions);
	}
}
