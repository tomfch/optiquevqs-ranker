package eu.optiquevqs.ranker.rankers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.ranker.association.Apriori;
import eu.optiquevqs.ranker.data_structures.AssociationRule;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.Extensions;
import eu.optiquevqs.ranker.data_structures.RatingsMatrix;
import eu.optiquevqs.ranker.data_structures.SimpleQuery;
import eu.optiquevqs.ranker.data_structures.SubClass;
import eu.optiquevqs.ranker.data_structures.SubClasses;
import eu.optiquevqs.ranker.enums.Category;
import eu.optiquevqs.ranker.enums.RecommendationType;
import eu.optiquevqs.ranker.data_structures.Item;
import eu.optiquevqs.ranker.utils.RankerUtils;

public class RulesModel extends Model {
	private Apriori apriori;
	private RatingsMatrix matrix;

	public RulesModel(Apriori apriori, Extensions extensions) {
		super(extensions);
		this.apriori = apriori;
		this.matrix = apriori.getRatingsMatrix();
	}

	@Override
	protected void calculateExtensionScores(List<Extension> extensionList, QueryGraph query, Category category) {
		List<Integer> itemIds = this.itemIdsFromQueryGraph(query);
		List<AssociationRule> relevantRules = this.findRelevantRules(itemIds, RecommendationType.EXTENSION);
		Collections.sort(relevantRules, Collections.reverseOrder());

		for(Extension extension : extensionList) {
			int extensionId = this.matrix.getIdFromTriple(extension.getTriple().getTopTriple());
			for(AssociationRule rule : relevantRules) {
				if(extensionId == rule.getConsequent()) {
					extension.increaseScore(rule.getConfidence());
					break;
				}
			}
		}
	}

	@Override
	protected void calculateSubClassScores(SubClasses subClasses, QueryGraph query, String focusType) {
		List<Integer> itemIds = this.itemIdsFromQueryGraph(query);
		List<AssociationRule> relevantRules = this.findRelevantRules(itemIds, RecommendationType.SUBCLASS);
		Collections.sort(relevantRules, Collections.reverseOrder());

		Iterator<SubClass> iter = subClasses.iterator();
		while(iter.hasNext()) {
			SubClass subClass = iter.next();
			int subClassItemId = this.matrix.getIdFromClass(subClass.getClassLabel());
			for(AssociationRule rule : relevantRules) {
				if(subClassItemId == rule.getConsequent()) {
					subClass.setScore(rule.getConfidence());
					break;
				}
			}
		}
	}

	private List<AssociationRule> findRelevantRules(List<Integer> itemIds, RecommendationType recommendationType) {
		return this.apriori.antecedentMatches(itemIds, recommendationType);
	}

	private List<Integer> itemIdsFromQueryGraph(QueryGraph query) {
		List<Integer> itemIds = new ArrayList<>();
		SimpleQuery simpleQuery = RankerUtils.queryGraphToSimpleQuery(query);
		Set<Item> itemSet = RankerUtils.simpleQueryToItemSet(simpleQuery, this.matrix);
		for(Item item : itemSet) itemIds.add(this.matrix.getIdFromItem(item));
		return itemIds;
	}
}
