package eu.optiquevqs.ranker.rankers;

import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.RatingsMatrix;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;

//NOT IN USE. Not updated according to ranker and model class hierarchy.
// Only for test purposes. Does not support real time recommendations.
public class FactorizationRanker extends Ranker {

	public FactorizationRanker(SimpleQueryLog querylog, RankerConfig config) {
		super(querylog, config);
	}

	public FactorizationRanker(QueryLog querylog, RankerConfig config) {
		super(querylog, config);
	}

	public Model createModel() {
		RatingsMatrix matrix = new RatingsMatrix(this.simpleQuerylog, 0.1, true);
		//return new FactorizationModel(matrix);
		return null;
	}
}
