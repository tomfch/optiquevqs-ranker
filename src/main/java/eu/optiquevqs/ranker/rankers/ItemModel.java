package eu.optiquevqs.ranker.rankers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.ranker.data_structures.RatingsMatrix;
import eu.optiquevqs.ranker.data_structures.SimpleQuery;
import eu.optiquevqs.ranker.data_structures.SubClass;
import eu.optiquevqs.ranker.data_structures.SubClasses;
import eu.optiquevqs.ranker.data_structures.Triple;
import eu.optiquevqs.ranker.enums.Category;
import eu.optiquevqs.ranker.neighborhood.Neighborhood;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.Extensions;
import eu.optiquevqs.ranker.data_structures.Item;
import eu.optiquevqs.ranker.data_structures.ItemClass;
import eu.optiquevqs.ranker.utils.RankerUtils;

public class ItemModel extends Model {
	private Neighborhood neighborhood;
	private RatingsMatrix matrix;

	public ItemModel(Neighborhood neighborhood, Extensions extensions) {
		super(extensions);
		this.matrix = neighborhood.getRatingsMatrix();
		this.neighborhood = neighborhood;
	}

	@Override
	protected void calculateExtensionScores(List<Extension> extensionList, QueryGraph query, Category category) {
		List<Integer> itemIds = this.itemIdsFromQueryGraph(query);
		for(Extension extension : extensionList) {
			Triple topTriple = extension.getTriple().getTopTriple();
			for(Integer itemId : itemIds) {
				Item item = this.matrix.getItemsFromId(itemId).iterator().next();
				if(RankerUtils.itemClassPartOfTriple(item, topTriple)) continue;
				double score = neighborhood.similarity(itemId, topTriple);
				extension.increaseScore(score);
			}
		}
	}

	@Override
	protected void calculateSubClassScores(SubClasses subClasses, QueryGraph query, String focusType) {
		Iterator<SubClass> iter = subClasses.iterator();
		List<Integer> itemIdsInQuery = itemIdsFromQueryGraph(query);
		while(iter.hasNext()) {
			SubClass subClass = iter.next();
			int subClassItemId = this.matrix.getIdFromClass(subClass.getClassLabel());
			double score = 0.0;
			for(Integer itemId : itemIdsInQuery) {
				Item item = this.matrix.getItemsFromId(itemId).iterator().next();
				if (item instanceof ItemClass && item.getLabel().equals(focusType)) continue; // exclude class item from similarity lookup
				score += this.neighborhood.similarity(itemId, subClassItemId);
			}
			subClass.setScore(score);
		}
	}

	private List<Integer> itemIdsFromQueryGraph(QueryGraph query) {
		List<Integer> itemIds = new ArrayList<>();
		SimpleQuery simpleQuery = RankerUtils.queryGraphToSimpleQuery(query);
		Set<Item> itemSet = RankerUtils.simpleQueryToItemSet(simpleQuery, this.matrix);
		for(Item item : itemSet) itemIds.add(this.matrix.getIdFromItem(item));
		return itemIds;
	}
}
