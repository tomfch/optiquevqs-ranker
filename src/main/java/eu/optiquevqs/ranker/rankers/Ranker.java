package eu.optiquevqs.ranker.rankers;

import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.utils.RankerUtils;

public abstract class Ranker {
	protected SimpleQueryLog simpleQuerylog;
	protected RankerConfig config;

	public Ranker(SimpleQueryLog simpleQuerylog, RankerConfig config) {
		this.simpleQuerylog = simpleQuerylog;
		this.config = config;
	}

	public Ranker(QueryLog querylog, RankerConfig config) {
		this(RankerUtils.queryLogToSimpleQueryLog(querylog), config);
	}

	public SimpleQueryLog getSimpleQueryLog() {
		return this.simpleQuerylog;
	}

	public abstract Model createModel();
}
