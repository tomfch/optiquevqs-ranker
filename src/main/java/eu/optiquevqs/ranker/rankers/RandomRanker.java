package eu.optiquevqs.ranker.rankers;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.enums.Category;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.Extensions;

/* All extensions are given random score. 
 *
 * If an object property has a self loop in the navigation graph (same range and domain), 
 * the same item will be recommended twice in the same list, one non-inverted entry and
 * one inverted. Pairs of the same item in the same list will be given the same random score.
 */
public class RandomRanker extends Ranker {

	public RandomRanker(SimpleQueryLog querylog, RankerConfig config) {
		super(querylog, config);
	}

	public RandomRanker(QueryLog querylog, RankerConfig config) {
		super(querylog, config);
	}

	@Override
	public Model createModel() {
		Extensions extensions = this.simpleQuerylog.getExtensions();
		Map<Category, Map<String, List<Extension>>> extensionMap = extensions.getExtensionMap();
		for(Map<String, List<Extension>> innerMap : extensionMap.values()) {
			for(List<Extension> extensionList : innerMap.values()) {
				this.randomScore(extensionList);
				this.sameScoreForInverses(extensionList);
			}
		}
		extensions.sort();
		return new RandomModel(extensions);
	}

	private void randomScore(List<Extension> extensionList) {
		for(Extension extension : extensionList) {
			extension.setScore(Math.random());
		}
	}

	private void sameScoreForInverses(List<Extension> extensionList) {
		Set<Extension> inverseExtensions = new HashSet<>();
		for(Extension extension : extensionList) {
			if(extension.isInverted()) inverseExtensions.add(extension);
		}
		for(Extension inverseExtension : inverseExtensions) {
			double randomScore = Math.random();
			for(Extension extension : extensionList) {
				if(extension.inverseOf(inverseExtension) || extension.equals(inverseExtension)) {
					extension.setScore(randomScore);
				}
			}
		}
	}
}
