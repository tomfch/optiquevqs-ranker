package eu.optiquevqs.ranker.rankers;

import java.util.Collections;
import java.util.List;

import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.Extensions;
import eu.optiquevqs.ranker.data_structures.SubClasses;
import eu.optiquevqs.ranker.enums.Category;

public abstract class Model {
	protected Extensions extensions;

	public Model(Extensions extensions) {
		this.extensions = extensions;
	}

	public List<Extension> recommendExtensions(QueryGraph query, String focusType, Category category) {
		focusTypeInQueryVerification(focusType, query);
		List<Extension> extensionList = this.getExtensionList(category, focusType);
		this.calculateExtensionScores(extensionList, query, category);
		this.sortExtensions(extensionList);
		return extensionList;
	}

	public SubClasses recommendSubClasses(QueryGraph query, String focusType) {
		focusTypeInQueryVerification(focusType, query);
		SubClasses subClasses = new SubClasses(query.getNavigationGraph(), focusType);
		this.calculateSubClassScores(subClasses, query, focusType);
		subClasses.sort();
		return subClasses;
	}

	protected void focusTypeInQueryVerification(String focusType, QueryGraph query) {
		if (query.containsClass(focusType)) return;
		throw new IllegalArgumentException(focusType + " is not a class in the query.");
	}

	protected List<Extension> getExtensionList(Category category, String focusType) {
		switch(category) {
			case DATATYPE :
				return this.extensions.allDatatypeExtensionsFrom(focusType);
			case OBJECT :
				return this.extensions.allObjectExtensionsFrom(focusType);
			default : 
				throw new IllegalArgumentException(category + " is not a valid category. Use \"object\" or \"datatype\"");
		}
	}

	protected abstract void calculateExtensionScores(List<Extension> extensionList, QueryGraph query, Category category);
	protected abstract void calculateSubClassScores(SubClasses subClasses, QueryGraph query, String focusType);

	protected void sortExtensions(List<Extension> extensionList) {
		Collections.sort(extensionList, Collections.reverseOrder());
	}

	public void deleteThisMethod() {
		extensions.prettyPrint();
	}
}
