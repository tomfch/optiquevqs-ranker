package eu.optiquevqs.ranker.rankers;

import eu.optiquevqs.ranker.association.Apriori;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.data_structures.Extensions;

public class RulesRanker extends Ranker {

	public RulesRanker(SimpleQueryLog querylog, RankerConfig config) {
		super(querylog, config);
	}

	public RulesRanker(QueryLog querylog, RankerConfig config) {
		super(querylog, config);
	}

	@Override
	public Model createModel() {
		Apriori apriori = new Apriori(this.simpleQuerylog, this.config);
		apriori.mineAssociationRules(); 
		Extensions extensions = this.simpleQuerylog.getExtensions();
		return new RulesModel(apriori, extensions);
	}
}
