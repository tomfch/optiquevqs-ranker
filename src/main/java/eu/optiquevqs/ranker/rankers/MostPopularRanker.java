package eu.optiquevqs.ranker.rankers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.data_structures.Triple;
import eu.optiquevqs.ranker.enums.Category;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.Extensions;

public class MostPopularRanker extends Ranker {

	public MostPopularRanker(SimpleQueryLog simpleQuerylog, RankerConfig config) {
		super(simpleQuerylog, config);
	}

	public MostPopularRanker(QueryLog querylog, RankerConfig config) {
		super(querylog, config);
	}

	@Override
	public Model createModel() {
		Map<Triple, Double> itemFrequencies = new HashMap<>();
		itemFrequencies.putAll(this.simpleQuerylog.getDataPropertyItems());
		itemFrequencies.putAll(this.simpleQuerylog.getObjectPropertyItems());
		Extensions extensions = this.simpleQuerylog.getExtensions();

		Map<Category, Map<String, List<Extension>>> extensionMap = extensions.getExtensionMap();
		for(Map<String, List<Extension>> innerMap : extensionMap.values()) {
			for(List<Extension> extensionList : innerMap.values()) {
				for(Extension extension : extensionList) {
					this.setScoreToFrequency(extension, itemFrequencies);
				}
			}
		}
		extensions.sort();
		return new MostPopularModel(extensions, this.simpleQuerylog.getClassFrequency());
	}

	private void setScoreToFrequency(Extension extension, Map<Triple, Double> itemFrequencies) {
		Triple item = extension.getTriple();
		try {
			extension.setScore(itemFrequencies.get(item));
		} catch (NullPointerException e) { // Inverse items
			for(Entry<Triple, Double> entry : itemFrequencies.entrySet()) {
				Triple currentItem = entry.getKey();
				double frequency = entry.getValue();
				if(item.inverseOf(currentItem)) {
					extension.setScore(frequency);
					return;
				}
			}
		}
	}
}
