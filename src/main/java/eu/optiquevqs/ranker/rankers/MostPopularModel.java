package eu.optiquevqs.ranker.rankers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.Extensions;
import eu.optiquevqs.ranker.data_structures.SubClass;
import eu.optiquevqs.ranker.data_structures.SubClasses;
import eu.optiquevqs.ranker.enums.Category;

public class MostPopularModel extends Model {
	private Map<String, Double> classFrequency;
	public MostPopularModel(Extensions extensions, Map<String, Double> classFrequency) {
		super(extensions);
		this.classFrequency = classFrequency;
	}

	@Override
	// No calculation to be done. Scores are query independent and already calculated in MostPopularRanker.
	protected void calculateExtensionScores(List<Extension> extensionList, QueryGraph query, Category category) {}

	@Override
	protected void calculateSubClassScores(SubClasses subClasses, QueryGraph query, String focusType) {
		Iterator<SubClass> iter = subClasses.iterator();
		while(iter.hasNext()) {
			SubClass subClass = iter.next();
			subClass.setScore(this.classFrequency.get(subClass.getClassLabel()));
		}
	}
}

