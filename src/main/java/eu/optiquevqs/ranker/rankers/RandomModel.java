package eu.optiquevqs.ranker.rankers;

import java.util.Iterator;
import java.util.List;

import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.Extensions;
import eu.optiquevqs.ranker.data_structures.SubClass;
import eu.optiquevqs.ranker.data_structures.SubClasses;
import eu.optiquevqs.ranker.enums.Category;

public class RandomModel extends Model {
	public RandomModel(Extensions extensions) {
		super(extensions);
	}

	@Override
	// No calculation to be done. Scores already randomized from RanomRanker.
	protected void calculateExtensionScores(List<Extension> extensionList, QueryGraph query, Category category) {}

	@Override
	protected void calculateSubClassScores(SubClasses subClasses, QueryGraph query, String focusType) {
		Iterator<SubClass> iter = subClasses.iterator();
		while(iter.hasNext()) iter.next().setScore(Math.random());
	}
}
