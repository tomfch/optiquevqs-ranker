package eu.optiquevqs.ranker.association;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.DoubleStream;

import eu.optiquevqs.ranker.data_structures.AssociationRule;
import eu.optiquevqs.ranker.data_structures.Item;
import eu.optiquevqs.ranker.data_structures.ItemClass;
import eu.optiquevqs.ranker.data_structures.ItemProperty;
import eu.optiquevqs.ranker.data_structures.Pattern;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.RatingsMatrix;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.enums.RecommendationType;
import eu.optiquevqs.ranker.enums.SimilarityMeasure;
import eu.optiquevqs.ranker.utils.RankerUtils;

// Provides a simple apriori algorithm for frequent pattern and association rules mining.
public class Apriori {

	private SimpleQueryLog querylog;
	private RatingsMatrix matrix;
	private RankerConfig config;
	private List<List<Pattern>> frequentPatterns; // Index out outer list corresponds to size of item set.
	private List<AssociationRule> extensionRules;
	private List<AssociationRule> subclassRules;
	private double absoluteMinSupport;

	public Apriori(SimpleQueryLog querylog, RankerConfig config) {
		this.querylog = querylog;
		this.config = config;
		this.matrix = new RatingsMatrix(this.querylog, config.decay, config.itemPropertyOnly);
		if(config.useInverseUserFrequency) this.matrix.useInverseUserFrequency();
		this.frequentPatterns = new ArrayList<>();
		this.extensionRules = new ArrayList<>();
		this.subclassRules = new ArrayList<>();
		this.absoluteMinSupport = config.minSupport * this.matrix.getTotWeight();
	}

	public Apriori(QueryLog querylog, RankerConfig config) {
		this(RankerUtils.queryLogToSimpleQueryLog(querylog), config);
	}

	public RatingsMatrix getRatingsMatrix() {
		return this.matrix;
	}

	public List<List<Pattern>> getFrequentPatterns() {
		return this.frequentPatterns;
	}

	/*
	 * Mines all association rules with exactly one item in the consequent, given minSupport and minConfidence.
	 * Populates this.associationRules and returns the list of association rules.
	 */
	public void mineAssociationRules() {
		List<AssociationRule> associationRules = new ArrayList<>();
		this.mineFrequentPatterns(this.absoluteMinSupport);
		for(int i = 2; i < this.frequentPatterns.size(); i++) {
			List<Pattern> patterns = this.frequentPatterns.get(i);
			for(Pattern pattern : patterns) {
				double itemsetSupport = pattern.getSupport();
				for(Integer itemId : pattern.getPattern()) {
					Pattern antecedent = new Pattern(pattern);
					antecedent.removeItem(itemId);
					double antecedentSupport = this.countSupport(antecedent.getPattern());
					antecedent.setSupport(antecedentSupport);
					double confidence = itemsetSupport / antecedentSupport;
					if(confidence >= config.minConfidence) {
						associationRules.add(new AssociationRule(antecedent, itemId, confidence));
					}
				}
			}
		}
		for(AssociationRule rule : associationRules) {
			Item consequent = this.matrix.getItemsFromId(rule.getConsequent()).iterator().next();
			if (consequent instanceof ItemClass) {
				this.subclassRules.add(rule);
			} else if (consequent instanceof ItemProperty) {
				if(this.typeOverlap(rule)) continue;
				this.extensionRules.add(rule);
			}
		}
	}

	/* 
	 * Mines all frequent patterns with support >= minSupport.
	 * Populates this.frequentPatterns and returns a list of lists of frequent patterns.
	 * Outer list index corresponds to the number of items in the frequent pattern.
	 */
	public List<List<Pattern>> mineFrequentPatterns(double minSupport) {
		this.frequentPatterns.add(new ArrayList<>()); // Skip index 0

		// Frequent patterns, size 1
		this.frequentPatterns.add(this.sizeOne(minSupport));

		// Frequent patterns, size >1
		for(int i = 2; i < matrix.getItemMap().size(); i++) {
			List<Pattern> sizeN = sizeN(minSupport, this.frequentPatterns, i);
			if(sizeN.isEmpty()) break;
			this.frequentPatterns.add(sizeN);
		}

		return this.frequentPatterns;
	}

	/*
	 * Mines all frequent patterns of a given size with support >= minSupport.
	 * 
	 * Args:
	 * 		minSupport: Minimum support.
	 * 		frequentPatterns: Patterns mined so far. Must contain mined patterns of size = size - 1.
	 * 		size: Number of items in frequent patterns to mine.
	 * 
	 * Return:
	 *		List of all frequent patterns with support >= minSupport, and number of items = size.
	 */
	private List<Pattern> sizeN(double minSupport, List<List<Pattern>> frequentPatterns, int size) {
		List<Pattern> nextLayer = new ArrayList<>();
		List<Pattern> prevLayer = frequentPatterns.get(size - 1);
		int prevLayerSize = prevLayer.size();
		for(int i = 0; i < prevLayerSize - 1; i++) {
			for(int j = i + 1; j < prevLayerSize; j++) {
				List<Integer> a = prevLayer.get(i).getPattern();
				List<Integer> b = prevLayer.get(j).getPattern();
				if(samePrefix(a, b)) {
					List<Integer> union = this.sortedUnion(a, b);
					double support = this.countSupport(union);
					if(support >= minSupport) {
						Pattern pattern = new Pattern(union, support);
						nextLayer.add(pattern);
					}
				}
			}
		}
		return nextLayer;
	}

	// Count support for a list of item id's. Assumes unique id's.
	private double countSupport(List<Integer> itemPattern) {
		double support = 0.0;
		int queryCount = this.matrix.queryCount();
		double[][] ratingsMatrix = this.matrix.getMatrix();
		for(int queryNumber = 0; queryNumber < queryCount; queryNumber++) {
			double lowestRating = Double.MAX_VALUE;
			boolean allItemsPresent = true;
			for(Integer itemId : itemPattern) {
				double rating = ratingsMatrix[queryNumber][itemId];
				if(rating <= 0.0) {
					allItemsPresent = false;
					break;
				} else if(rating < lowestRating) {
					lowestRating = rating;
				}
			}
			if(allItemsPresent) support += this.matrix.getWeights()[queryNumber] * lowestRating; 
		}
		return support;
	}

	// Checks if k - 1 prefix is identical. Assumes a and b are of equal size.
	private boolean samePrefix(List<Integer> a, List<Integer> b) {
		return a.subList(0, a.size() - 1).equals(b.subList(0, a.size() - 1));
	}

	/*
	 *  Returns sorted union of two lists of item ids.
	 *  Assumes both lists are sorted, are of equal size and have share the first n - 1 elements.
	 */
	private List<Integer> sortedUnion(List<Integer> a, List<Integer> b) {
		List<Integer> union = new ArrayList<>(a);
		union.add(b.get(b.size() -1));
		return union;
	}

	// Count item sets of size one.
	private List<Pattern> sizeOne(double minSupport) {
		List<Pattern> sizeOne = new ArrayList<>();
		double[][] matrixTranspose = this.matrix.getMatrixTranspose();
		/*
		int i = 0;
		for(double[] a : matrixTranspose) {
			double sum = DoubleStream.of(a).sum();
			if(sum >= minSupport) {
				List<Integer> pattern = new ArrayList<>();
				pattern.add(i);
				sizeOne.add(new Pattern(pattern, sum));
			}
			i++;
		}
		*/
		for(int i = 0; i < matrixTranspose.length; i++) {
			double weightSum = 0.0;
			for(int j = 0; j < matrixTranspose[0].length; j++) {
				weightSum += RankerUtils.zeroOrOne(matrixTranspose[i][j]) * this.matrix.getWeights()[j];
			}
			if(weightSum >= this.absoluteMinSupport) {
				List<Integer> pattern = new ArrayList<>();
				pattern.add(i);
				sizeOne.add(new Pattern(pattern, weightSum));
			}
		}
		return sizeOne;
	}

	// Returns all association rules that contain one or more items from itemIds in its antecedent.
	public List<AssociationRule> antecedentMatches(List<Integer> itemIds, RecommendationType recommendationType) {
		List<AssociationRule> antecedentMatches = new ArrayList<>();
		List<AssociationRule> rulesList = null;
		switch(recommendationType) {
			case EXTENSION :
				rulesList = this.extensionRules;
				break;
			case SUBCLASS:
				rulesList = this.subclassRules;
				break;
		}

		for(AssociationRule rule : rulesList) {
			if(rule.antecedentMatch(itemIds)) antecedentMatches.add(rule);
		}
		return antecedentMatches;
	}

	/*
	 * For all items in antecedent: if item is a class, check if type is
	 * the type of object- or subject position of item in consequent. Returns
	 * true if there is any overlap, false otherwise.
	 */
	private boolean typeOverlap(AssociationRule rule) {
		int consequentId = rule.getConsequent();
		ItemProperty consequent = (ItemProperty)this.matrix.getItemsFromId(consequentId).iterator().next();
		String subjectType = consequent.getTriple().getSubject();
		String objectType = consequent.getTriple().getObject();
		Pattern antecedent = rule.getAntecedent();
		for(Integer itemId : antecedent.getPattern()) {
			Item item = this.matrix.getItemsFromId(itemId).iterator().next();
			if(item instanceof ItemProperty) continue;
			String type = item.getLabel();
			if(type.equals(subjectType)) return true;
			if(type.equals(objectType)) return true;
		}
		return false;
	}
}
