package eu.optiquevqs.ranker.neighborhood;

import java.util.HashMap;
import java.util.Map;

import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.RatingsMatrix;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.data_structures.Triple;
import eu.optiquevqs.ranker.enums.SimilarityMeasure;
import eu.optiquevqs.ranker.utils.RankerUtils;

public class Neighborhood {
	private RatingsMatrix matrix;
	RankerConfig config;
	private Map<Integer, Map<Integer, Double>> similarityMap; // <itemdId1, <itemdId2, double>>

	public Neighborhood(SimpleQueryLog simpleQuerylog, RankerConfig config) {
		this.config = config;
		this.matrix = new RatingsMatrix(simpleQuerylog, config.decay, config.itemPropertyOnly);
		if(config.useInverseUserFrequency) this.matrix.useInverseUserFrequency();
		this.populateSimilarityMap();
	}

	public RatingsMatrix getRatingsMatrix() {
		return this.matrix;
	}

	public double similarity(int itemId, Triple triple) {
		return similarity(itemId, this.matrix.getIdFromTriple(triple));
	}

	public double similarity(int itemId, String classLabel) {
		return similarity(itemId, this.matrix.getIdFromClass(classLabel));
	}

	public double similarity(int itemId1, int itemId2) {
		return this.similarityMap.get(itemId1).get(itemId2);
	}

	private void populateSimilarityMap() {
		this.similarityMap = new HashMap<>();
		double[][] matrixTransposed = this.matrix.getMatrixTranspose();
		double[] weights = this.matrix.getWeights();

		for (int i = 0; i < this.matrix.itemCount(); i++) {
			this.similarityMap.put(i, new HashMap<>());
			this.similarityMap.get(i).put(i, 0.0); // Sets similarity to self = 0.0
		}

		for (int i = 0; i < this.matrix.itemCount(); i++) {
			for (int j = i + 1; j < this.matrix.itemCount(); j++) {
				double similarityScore = 0.0;
				double[] v1 = matrixTransposed[i];
				double[] v2 = matrixTransposed[j];

				switch(this.config.similarityMeasure) {
					case COSINE :
						similarityScore = RankerUtils.cosSimilarityModified(v1, v2, weights);
						break;
					case EUCLIDIAN :
						similarityScore = RankerUtils.euclidianDistanceModified(v1, v2, weights);
						break;
					case JACCARD_OLD :
						similarityScore = RankerUtils.jaccardIndexOldModified(v1, v2, weights);
						break;
					case RUZICKA:
						similarityScore = RankerUtils.ruzickaModified(v1, v2, weights);
						break;
					case JACCARD:
						similarityScore = RankerUtils.jaccardIndexModified(v1, v2, weights);
						break;
					case DICE:
						similarityScore = RankerUtils.diceSimilarityModified(v1, v2, weights);
						break;
					default :
						String msg = this.config.similarityMeasure + " is not a valid similarity mesaure.";
						throw new IllegalArgumentException(msg);
				}

				this.similarityMap.get(i).put(j, similarityScore);
				this.similarityMap.get(j).put(i, similarityScore);
			}
		}
	}
}
