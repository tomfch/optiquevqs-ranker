package eu.optiquevqs.ranker.data_structures;

import eu.optiquevqs.ranker.enums.RecommendationType;

public class Results {
	private RecommendationType recommendationType;
	private int numberOfRuns;
	private double ARHRaccumulated;
	private double RHRstdevAccumulated;
	private double avgRankAccumulated;
	private double rankStdevAccumulated;

	public Results(RecommendationType recommendationType) {
		this.recommendationType = recommendationType;
		this.numberOfRuns = 0;
		this.ARHRaccumulated = 0.0;
		this.RHRstdevAccumulated = 0.0;
		this.avgRankAccumulated = 0.0;
		this.rankStdevAccumulated = 0.0;
	}

	public void add(ResultPerRun resultPerRun) {
		this.numberOfRuns++;
		this.ARHRaccumulated += resultPerRun.getARHR();
		this.RHRstdevAccumulated += resultPerRun.getRHRstdev();
		this.avgRankAccumulated += resultPerRun.getAvgRank();
		this.rankStdevAccumulated += resultPerRun.getRankStdev();
	}

	public RecommendationType getRecommendationType() {
		return this.recommendationType;
	}

	public double getAvgARHR() {
		return this.ARHRaccumulated / this.numberOfRuns;
	}

	public double getARHRstdev() {
		return this.RHRstdevAccumulated / this.numberOfRuns;
	}

	public double getAvgAvgRank() {
		return this.avgRankAccumulated / this.numberOfRuns;
	}
	public double getAvgRankStdev() {
		return this.rankStdevAccumulated / this.numberOfRuns;
	}

}
