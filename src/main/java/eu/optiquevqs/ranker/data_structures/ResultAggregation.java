package eu.optiquevqs.ranker.data_structures;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import eu.optiquevqs.ranker.enums.RecommendationType;

public class ResultAggregation {
	private Set<ResultPerRecommend> resultsPerRecommend;

	private RecommendationType recommendationType;
	private int queryCount;
	private double weightAccumulated;
	private double rankTimesWeightAccumulated;
	private double RHRtimesWeightAccumulated;
	private Map<Integer, Double> rankWeightDistribution; // <rank, weight>

	private double ARHR;
	private double RHRstdev;
	private double avgRank;
	private double rankStdev;

	public ResultAggregation(RecommendationType recommendationType) {
		this.resultsPerRecommend = new HashSet<>();
		this.recommendationType = recommendationType;
		this.queryCount = 0;
		this.weightAccumulated = 0.0;
		this.rankTimesWeightAccumulated = 0.0;
		this.RHRtimesWeightAccumulated = 0.0;
		this.rankWeightDistribution = new HashMap<>();
		this.ARHR = -1.0;
		this.RHRstdev = -1.0;
		this.avgRank = -1.0;
		this.rankStdev = -1.0;
	}

	public void add(ResultPerRecommend res) {
		this.resultsPerRecommend.add(res);
		this.weightAccumulated += res.weight;
		this.queryCount++;
		this.rankTimesWeightAccumulated += res.weight * res.rank;
		this.RHRtimesWeightAccumulated += res.weight * res.RHR;
		this.ARHR = -1.0;
		this.RHRstdev = -1.0;
		this.avgRank = -1.0;
		this.rankStdev = -1.0;

		double oldRankWeight = 0.0;
		if(this.rankWeightDistribution.containsKey(res.rank)) {
			oldRankWeight = this.rankWeightDistribution.get(res.rank);
		}
		this.rankWeightDistribution.put(res.rank, oldRankWeight + res.weight);
	}

	public RecommendationType getRecommendationType() {
		return this.recommendationType;
	}

	public double getWeightAccumulated() {
		return this.weightAccumulated;
	}

	public double getQueryCount() {
		return this.queryCount;
	}

	public Map<Integer, Double> getRankWeightDistribution() {
		return this.rankWeightDistribution;
	}

	public double getARHR() {
		if(this.ARHR >= 0) return this.ARHR;
		this.ARHR = RHRtimesWeightAccumulated / weightAccumulated;
		return this.ARHR;
	}

	public double getRHRstdev() {
		if(this.RHRstdev >= 0) return this.RHRstdev;
		double RHRstdev = 0.0;
		double ARHR = this.getARHR();
		for(ResultPerRecommend res : this.resultsPerRecommend) {
			RHRstdev += res.weight * Math.pow(res.RHR - ARHR, 2);
		}
		RHRstdev = RHRstdev / (this.weightAccumulated - 1);
		this.RHRstdev = RHRstdev;
		return this.RHRstdev;
	}

	public double getAvgRank() {
		if(this.avgRank >= 0) return this.avgRank;
		this.avgRank = rankTimesWeightAccumulated / weightAccumulated;
		return this.avgRank;
	}

	public double getRankStdev() {
		if(this.rankStdev >= 0) return this.rankStdev;
		double rankStdev = 0.0;
		double avgRank = this.getAvgRank();
		for(ResultPerRecommend res : this.resultsPerRecommend) {
			rankStdev += res.weight * Math.pow(res.rank - avgRank, 2);
		}
		rankStdev = Math.sqrt(rankStdev / (this.weightAccumulated - 1));
		this.rankStdev = rankStdev;
		return this.rankStdev;
	}

}
