package eu.optiquevqs.ranker.data_structures;

import java.util.HashSet;
import java.util.Set;

import eu.optiquevqs.ranker.enums.Category;

// A simplified set representation of queries. Variable names, paths and duplicate items are ignored.
public class SimpleQuery {
	private Set<Triple> query;
	private double weight;

	public SimpleQuery() {
		this.query = new HashSet<>();
		this.weight = 1.0;
	}
		
	public Set<Triple> getQuery() {
		return this.query;
	}

	public double getWeight() {
		return this.weight;
	}

	public void addTriple(String left, String middle, String right, Category category) {
		this.query.add(new Triple(left, middle, right, category));
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int itemCount() {
		return this.query.size();
	}

	public Set<String> getClasses() {
		Set<String> classes = new HashSet<>();
		for(Triple triple : this.query) {
			classes.add(triple.getSubject());
			if(triple.getCategory() == Category.OBJECT) classes.add(triple.getObject());
		}
		return classes;
	}
}
