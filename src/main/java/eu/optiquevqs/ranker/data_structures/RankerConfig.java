package eu.optiquevqs.ranker.data_structures;

import eu.optiquevqs.ranker.enums.RankerType;
import eu.optiquevqs.ranker.enums.SimilarityMeasure;

public class RankerConfig {
	public RankerType rankerType;
	public double minSupport; // For rules ranker
	public double minConfidence; // For rules ranker
	public SimilarityMeasure similarityMeasure; // For rules and item ranker
	public double decay; // For rules and item ranker
	public boolean itemPropertyOnly; // If true: only consider properties in rating matrix, not classes.
	public boolean useInverseUserFrequency = false;
}
