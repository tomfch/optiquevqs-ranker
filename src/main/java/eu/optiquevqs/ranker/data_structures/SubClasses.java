package eu.optiquevqs.ranker.data_structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import eu.optiquevqs.graph.navigation.NavigationGraph;

public class SubClasses {
	List<SubClass> subClasses;

	public SubClasses(NavigationGraph navigationGraph, String focusNodeType) {
		this.subClasses = new ArrayList<>();
		Set<String> subClassLabels = navigationGraph.getSubClasses(focusNodeType);
		for(String subClassLabel : subClassLabels) subClasses.add(new SubClass(subClassLabel));
	}

	public Iterator<SubClass> iterator() {
		return this.subClasses.iterator();
	}

	public int size() {
		return this.subClasses.size();
	}

	public void sort() {
		Collections.sort(subClasses, Collections.reverseOrder());
	}

	public int rankOf(String subclassLabel) {
		int i = 1;
		for(SubClass subclass : subClasses) {
			if(subclass.getClassLabel().equals(subclassLabel)) return i;
			i++;
		}
		throw new NoSuchElementException(subclassLabel + " is not in list of recommended subclasses.");
	}

	public String topRankedClass() {
		return this.iterator().next().getClassLabel();
	}

	public String top3() {
		String top = "";
		Iterator<SubClass> it = this.iterator();
		int i = 0;
		while(it.hasNext()) {
			if(i > 2) break;
			top += it.next() + ", ";
			i++;
		}
		return top.substring(0, top.length() - 2);
	}

	public void prettyPrint() {
		for(SubClass subClass : this.subClasses) System.out.println(subClass);
	}
}
