package eu.optiquevqs.ranker.data_structures;

import java.util.Iterator;
import java.util.List;

/* 
 * Association rules representation. Only supports rules with exactly one item in the consequent.
 * Association rule: X -> Y, where X is antecedent and Y is consequent.
 */
public class AssociationRule implements Comparable<AssociationRule> {
	private Pattern antecedent;
	private int consequent; // itemId
	private double confidence;

	public AssociationRule(Pattern antecedent, int consequent, double confidence) {
		this.antecedent = antecedent;
		this.consequent = consequent;
		this.confidence = confidence;
	}

	public Pattern getAntecedent() {
		return this.antecedent;
	}

	// Returns itemId of consequent
	public int getConsequent() {
		return this.consequent;
	}

	public double getConfidence() {
		return this.confidence;
	}
	
	@Override
	public String toString() {
		return this.antecedent + " => [" + this.consequent + "] confidence: " + this.confidence;
	}

	/*
	 *  Returns true if one or more item ids in itemIds is present in antecedent.
	 *  Assumes non-empty and sorted lists - both for itemsIds and antecedent.
	 */
	public boolean antecedentMatch(List<Integer> itemIds) {
		Iterator<Integer> antecedentItemsIterator = this.antecedent.getPattern().iterator();
		Iterator<Integer> itemIdsIterator = itemIds.iterator();
		int a = itemIdsIterator.next();
		int b = antecedentItemsIterator.next();
		while(antecedentItemsIterator.hasNext() || a < b) {
			if (!itemIdsIterator.hasNext()) {
				return false;
			} else {
				if (a < b) {
					a = itemIdsIterator.next();
				} else if (a > b) {
					return false;
				} else if (a == b) {
					a = itemIdsIterator.next();
					b = antecedentItemsIterator.next();
				}
			}
		}
		if (a == b) return true;
		return false;
	}

	@Override
	public int compareTo(AssociationRule another) {
		return Double.compare(this.confidence, another.getConfidence());
	}

}
