package eu.optiquevqs.ranker.data_structures;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.ranker.enums.Category;

public class SimpleQueryLog {
	private NavigationGraph navigationGraph;
	private Set<SimpleQuery> queries;
	private Extensions extensions;

	// Global stats
	private Map<Triple, Double> objectPropertyItemFrequency;
	private Map<Triple, Double> datatypePropertyItemFrequency; 
	private Map<String, Double> classFrequency;
	private Map<String, Double> datatypeFrequency;

	public SimpleQueryLog(NavigationGraph navigationGraph) {
		this.navigationGraph = navigationGraph;
		this.extensions = new Extensions(navigationGraph);
		this.queries = new HashSet<>();
		this.objectPropertyItemFrequency = new HashMap<>();
		this.datatypePropertyItemFrequency = new HashMap<>();
		this.classFrequency = new HashMap<>();
		this.datatypeFrequency = new HashMap<>();
		this.prepareGlobalStats();
	}

	public Set<SimpleQuery> getQueries() {
		return this.queries;
	}
	public Map<Triple, Double> getObjectPropertyItems() {
		return this.objectPropertyItemFrequency;
	}

	public Map<Triple, Double> getDataPropertyItems() {
		return this.datatypePropertyItemFrequency;
	}

	public Extensions getExtensions() {
		return this.extensions;
	}

	public NavigationGraph getNavigationGraph() {
		return this.navigationGraph;
	}

	// Number of queries
	public int size() {
		return this.queries.size();
	}

	public Map<String, Double> getClassFrequency() {
		return this.classFrequency;
	}

	// Get all classes in navigation graph, including classes not used in any queries
	public Set<String> getAllClasses() {
		Set<String> classes = new HashSet<>();
		for (String c : this.classFrequency.keySet()) classes.add(c);
		return classes;
	}

	// Get all data types in navigation graph, including data types not used in any queries.
	public Set<String> getAllDatatypes() {
		Set<String> datatypes = new HashSet<>();
		for (String c : this.datatypeFrequency.keySet()) datatypes.add(c);
		return datatypes;
	}

	public boolean add(SimpleQuery simpleQuery) {
		return this.queries.add(simpleQuery);
	}

	public Set<Triple> getExtensionsFrom(String nodeType, Category category) {
		switch(category) {
			case DATATYPE :
				return this.datatypeExtensions(nodeType);
			case OBJECT :
				return this.objectExtensions(nodeType, true);
			default :
				throw new IllegalArgumentException(category + " is not a valid category. Use \"object\" or \"datatype\"");
		}
	}

	private Set<Triple> objectExtensions(String nodeType, boolean includeInvertedItems) {
		Set<Triple> extensions = new HashSet<>();
		Set<Pair<String, String>> extensionPairs = this.navigationGraph.getOutgoingObjectPairs(nodeType);
		for(Pair<String, String> pair : extensionPairs) {
			extensions.add(new Triple(nodeType, pair.getLeft(), pair.getRight(), Category.OBJECT));
		}
		if(includeInvertedItems) {
			Set<Pair<String, String>> inverseExtensionPairs = this.navigationGraph.getIncomingObjectPairs(nodeType);
			for(Pair<String, String> pair : inverseExtensionPairs) {
				extensions.add(new Triple(nodeType, pair.getLeft(), pair.getRight(), Category.OBJECT, true));
			}
		}
		return extensions;
	}

	private Set<Triple> datatypeExtensions(String nodeType) {
		Set<Triple> extensions = new HashSet<>();
		Set<Pair<String, String>> extensionPairs = this.navigationGraph.getOutgoingDatatypePairs(nodeType);
		for(Pair<String, String> pair : extensionPairs) {
			extensions.add(new Triple(nodeType, pair.getLeft(), pair.getRight(), Category.DATATYPE));
		}
		return extensions;
	}

	public boolean isObjectProperty(Triple triple) {
		return navigationGraph.isObjectProperty(triple.getPredicate());
	}

	public boolean isDatatypeProperty(Triple triple) {
		return navigationGraph.isDatatypeProperty(triple.getPredicate());
	}

	public Set<Triple> getAllTriples() {
		Set<Triple> triples = new HashSet<>();
		Set<String> classes = this.getAllClasses();
		for (String c : classes) {
			triples.addAll(this.datatypeExtensions(c));
			triples.addAll(this.objectExtensions(c, true));
		}
		return triples;
	}

	public Set<Triple> getAllNonInvertedTriples() {
		Set<Triple> triples = new HashSet<>();
		Set<String> classes = this.getAllClasses();
		for (String c : classes) {
			triples.addAll(this.datatypeExtensions(c));
			triples.addAll(this.objectExtensions(c, false));
		}
		return triples;
	}

	private void putItem(Triple triple, double weight) {
		if (this.isObjectProperty(triple)) {
			Double d = this.objectPropertyItemFrequency.putIfAbsent(triple, weight);
			if (d != null) {
				this.objectPropertyItemFrequency.replace(triple, d, d + weight);
			}
		} else if (this.isDatatypeProperty(triple)) {
			Double d = this.datatypePropertyItemFrequency.putIfAbsent(triple, weight);
			if (d != null) {
				this.datatypePropertyItemFrequency.replace(triple, d, d + weight);
			}
		}
	}

	private void incrementFrequency(String c, double weight) {
		if (this.classFrequency.containsKey(c)) {
			double currentValue = this.classFrequency.get(c);
			this.classFrequency.replace(c, currentValue + weight);
		} else if (this.datatypeFrequency.containsKey(c)) {
			double currentValue = this.datatypeFrequency.get(c);
			this.datatypeFrequency.replace(c, currentValue + weight);
		} else {
			throw new NoSuchElementException(c + " is not a class or a datatype");
		}
	}

	/*
	 *  Populates objectPropertyVectors, dataPropertyVectors, classFrequency and datatypeFrequency.
	 *  Must be run before a ranking model can be made.
	 */
	public void prepareGlobalStats() {
		Set<String> classes = this.navigationGraph.getAllClasses();
		for (String c : classes) {
			this.classFrequency.put(c, 0.0);
		}

		Set<String> datatypes = this.navigationGraph.getAllDatatypes();
		for (String dt : datatypes) {
			this.datatypeFrequency.put(dt, 0.0);
		}

		for(SimpleQuery simpleQuery : queries) {
			Set<Triple> triples = simpleQuery.getQuery();
			double weight = simpleQuery.getWeight();

			for (Triple triple : triples) {
				this.putItem(triple, weight);
				this.incrementFrequency(triple.getSubject(), weight);
				this.incrementFrequency(triple.getObject(), weight);
			}
		}
	}

	public boolean isLongTailProperty(Triple triple) {
		Category category = triple.getCategory();
		double frequency = -1.0;
		switch(category) {
			case DATATYPE:
				frequency = this.datatypePropertyItemFrequency.get(triple.getNonInvertedTriple());
				break;
			case OBJECT:
				frequency = this.objectPropertyItemFrequency.get(triple.getNonInvertedTriple());
				break;
		}
		if(frequency < 10000.0) return true;
		if(frequency < 0.0) throw new IllegalStateException("Not able to retrieve property frequency.");
		return false;
			
	}
}
