package eu.optiquevqs.ranker.data_structures;

import eu.optiquevqs.ranker.enums.Category;

public class Extension implements Comparable<Extension> {
	private Triple triple;
	private double score;

	public Extension(Triple triple) {
		this.triple = triple;
		this.score = 0.0;
	}

	public Extension(Triple triple, double score) {
		this.triple = triple;
		this.score = score;
	}

	// Copy constructor
	public Extension(Extension another) {
		this.triple = another.getTriple();
		this.score = another.getScore();
	}

	public Triple getTriple() {
		return this.triple;
	}

	public String getLeft() {
		return this.triple.getSubject();
	}

	public String getMiddle() {
		return this.triple.getPredicate();
	}

	public String getRight() {
		return this.triple.getObject();
	}

	public double getScore() {
		return this.score;
	}

	public Category getCategory() {
		return this.triple.getCategory();
	}

	public boolean isInverted() {
		return this.triple.isInverted();
	}

	public void setScore(double score) {
		this.score = score;
	}

	public void increaseScore(double scoreIncrease) {
		this.score += scoreIncrease;
	}

	public boolean inverseOf(Extension another) {
		return this.triple.inverseOf(another.getTriple());
	}

	@Override
	public int compareTo(Extension extension) {
		return Double.compare(this.score, extension.getScore());
	}

	@Override
	public String toString() {
		return this.triple + "    " + this.score;
	}
}
