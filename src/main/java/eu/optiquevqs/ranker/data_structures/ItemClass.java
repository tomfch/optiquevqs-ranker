package eu.optiquevqs.ranker.data_structures;

import java.util.Objects;

public class ItemClass extends Item {
	public ItemClass(String label) {
		super(label);
	}

	@Override
	public String toString() {
		return "[" + this.label + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof ItemClass)) return false;

		ItemClass v = (ItemClass)o;
		if (!v.getLabel().equals(this.label)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.label);
	}
}
