package eu.optiquevqs.ranker.data_structures;

import java.util.ArrayList;
import java.util.List;

// Representation of a pattern, i.e. item set. Items are represented with item ids.
public class Pattern {
	private List<Integer> pattern;
	private double support;

	public Pattern() {
		this.pattern = new ArrayList<>();
		this.support = 0.0;
	}

	public Pattern(List<Integer> pattern, double support) {
		this.pattern = pattern;
		this.support = support;
	}

	// Copy constructor
	public Pattern(Pattern another) {
		this.pattern = new ArrayList<>(another.getPattern());
		this.support = another.getSupport();
	}

	public List<Integer> getPattern() {
		return this.pattern;
	}

	public double getSupport() {
		return this.support;
	}

	public String toString() {
		return this.pattern.toString();
	}

	public void addItem(Integer itemId)  {
		this.pattern.add(itemId);
	}

	public void setSupport(double support) {
		this.support = support;
	}

	// Support not valid after pattern is reduced. Must be recalculated. 
	// Calling this method sets support to -1.0.
	public void removeItem(Integer itemId) {
		this.pattern.remove((Integer)itemId);
		this.support = -1.0;
	}

}
