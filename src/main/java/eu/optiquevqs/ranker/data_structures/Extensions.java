package eu.optiquevqs.ranker.data_structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.SubClassEdge;
import eu.optiquevqs.graph.navigation.DatatypeNode;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.navigation.NavigationGraphNode;
import eu.optiquevqs.ranker.enums.Category;

/*
 *  Provides access to all possible object property or datatype extensions for a given
 *  focus variable type. Possible extenstions are given by the navigation graph.
 */
public class Extensions {
	// <category, <focus node type, list of possible extensions>>
	private Map<Category, Map<String, List<Extension>>> extensionMap;

	public Extensions(NavigationGraph navigationGraph) {
		this.extensionMap = new HashMap<>();
		this.extensionMap.put(Category.DATATYPE, new HashMap<>());
		this.extensionMap.put(Category.OBJECT, new HashMap<>());
		this.populateExtensionMap(navigationGraph);
	}

	public Map<Category, Map<String, List<Extension>>> getExtensionMap() {
		return this.extensionMap;
	}

	/*
	 * Returns all possible datatype property extensions from a focus variable with given type.
	 * Returned list is a deep copy.
	 */
	public List<Extension> allDatatypeExtensionsFrom(String focusType) {
		//return this.extensionMap.get(Category.DATATYPE).get(focusType);
		List<Extension> extensionsCopy = new ArrayList<>();
		List<Extension> extensions = this.extensionMap.get(Category.DATATYPE).get(focusType);
		for(Extension extension : extensions) {
			extensionsCopy.add(new Extension(extension));
		}
		return extensionsCopy;
	}

	/* Returns all possible object property extensions from a focus variable with given type.
	 * Returned list is a deep copy.
	 */
	public List<Extension> allObjectExtensionsFrom(String focusType) {
		List<Extension> extensionsCopy = new ArrayList<>();
		List<Extension> extensions = this.extensionMap.get(Category.OBJECT).get(focusType);
		for(Extension extension : extensions) {
			extensionsCopy.add(new Extension(extension));
		}
		return extensionsCopy;
	}

	/*
	 * Add extensions for each subclass connection for each edge in the navigation graph.
	 * Also add inverse extensions for object properties.
	 */
	private void populateExtensionMap(NavigationGraph navigationGraph) {
		for(PropertyEdge edge : navigationGraph.edgeSet()) {
			if(edge instanceof SubClassEdge) continue;
			NavigationGraphNode source = navigationGraph.getEdgeSource(edge);
			NavigationGraphNode target = navigationGraph.getEdgeTarget(edge);
			String sourceLabel = source.getLabel();
			String edgeLabel = edge.getLabel();
			String targetLabel = target.getLabel();
			//System.out.println(sourceLabel + " " + edgeLabel + " " + targetLabel);
			Set<String> sources = navigationGraph.getSubClasses(sourceLabel);
			Set<String> targets = navigationGraph.getSubClasses(targetLabel);

			boolean isDatatypeProperty = target instanceof DatatypeNode;
			Category category = isDatatypeProperty ? Category.DATATYPE : Category.OBJECT;
			Triple topTriple = new Triple(sourceLabel, edgeLabel, targetLabel, category);

			for(String s : sources) {
				Extension extension = new Extension(new Triple(s, edgeLabel, targetLabel, category, topTriple));
				List<Extension> extensionList = this.extensionMap.get(category).get(s);
				if(extensionList == null) this.extensionMap.get(category).put(s, new ArrayList<>());
				this.extensionMap.get(category).get(s).add(extension);
				//System.out.println("  " + extension);
			}

			if(isDatatypeProperty) continue;

			for(String t : targets) {
				Extension extensionInverted = new Extension(new Triple(t, edgeLabel, sourceLabel, category, true, topTriple)); // need to create and use topTripleInverse??
				List<Extension> extensionInvertedList = this.extensionMap.get(category).get(t);
				if(extensionInvertedList == null) this.extensionMap.get(category).put(t, new ArrayList<>());
				this.extensionMap.get(category).get(t).add(extensionInverted);
				//System.out.println("  " + extensionInverted);
			}
		}
	}

	// Sorts all extensions lists at the innermost level of extensionMap.
	public void sort() {
		for(Map<String, List<Extension>> innerMap : extensionMap.values()) {
			for(List<Extension> extensionList : innerMap.values()) {
				Collections.sort(extensionList, Collections.reverseOrder());
			}
		}
	}

	public void prettyPrint() {
		for(Entry<Category, Map<String, List<Extension>>> e1 : this.extensionMap.entrySet()) {
			System.out.println(e1.getKey());
			for(Entry<String, List<Extension>> e2 : e1.getValue().entrySet()) {
				System.out.println("   " + e2.getKey());
				for(Extension e3 : e2.getValue()) {
					System.out.println("      " + e3);
				}
			}
		}	
	}
}
