package eu.optiquevqs.ranker.data_structures;

public abstract class Item {
	protected String label;

	public Item(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}
}