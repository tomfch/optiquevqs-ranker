package eu.optiquevqs.ranker.data_structures;

import eu.optiquevqs.ranker.enums.RecommendationType;

public class ResultPerRecommend {
	public RecommendationType recommendationType;
	public double weight;
	public double RHR;
	public int rank;

	@Override
	public String toString() {
		return "weight: " + this.weight + " rank: " + this.rank + " RHR: " + this.RHR;
	}
}
