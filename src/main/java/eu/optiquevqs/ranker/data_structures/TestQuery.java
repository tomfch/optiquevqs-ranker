package eu.optiquevqs.ranker.data_structures;

import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.ranker.enums.Category;

public class TestQuery {
	private QueryGraph query;
	private String focusNodeType;
	private Category category;
	private String correctProperty;
	private String targetType;
	private boolean isInverted;

	public TestQuery(QueryGraph query, String focusNodeType, Category category, String correctProperty, String targetType, boolean isInverted) {
		this.query = query;
		this.focusNodeType = focusNodeType;
		this.category = category;
		this.correctProperty = correctProperty;
		this.targetType = targetType;
		this.isInverted = isInverted;
	}

	public QueryGraph getQuery() { return this.query; }
	public String getFocusNodeType() { return this.focusNodeType; }
	public Category getCategory() { return this.category; }
	public String getCorrectProperty() { return this.correctProperty; }
	public double getWeight() {return this.query.getWeight(); }

	// Gets top superclass for extension 
	public String getCorrectClass() { 
		NavigationGraph ng = this.query.getNavigationGraph();
		if(category == Category.OBJECT) {
			String[] topClasses = ng.topSuperclasses(this.focusNodeType, this.correctProperty, this.targetType, this.isInverted);
			return this.isInverted ? topClasses[0] : topClasses[1];
		} 
		return targetType;
	}

	public Triple getCorrectTriple() {
		return new Triple(this.focusNodeType, this.correctProperty, this.getCorrectClass(), this.category, this.isInverted);
	}

	public String toString() {
		return this.query.toString();
	}
}
