package eu.optiquevqs.ranker.data_structures;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import eu.optiquevqs.graph.navigation.ClassHierarchyDistance;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.ranker.utils.RankerUtils;

// Rating matrix of query log. Queries with less than 2 edges are ignored.
public class RatingsMatrix {
	private SimpleQueryLog simpleQuerylog;
	ClassHierarchyDistance classHierarchyDistance;
	private double[][] matrix; // dimensions: queryCount, itemCount
	private double[] weights; // length: queryCount
	private Map<Integer, Set<Item>> itemMap;
	private Map<Item, Integer> inverseItemMap;
	private int itemCount;
	private int queryCount;
	private double decay;
	private double totWeight;
	private boolean itemPropertyOnly;

	public RatingsMatrix(SimpleQueryLog querylog, double decay, boolean itemPropertyOnly) {
		this.itemPropertyOnly = itemPropertyOnly;
		this.simpleQuerylog = querylog;
		this.decay = decay;
		this.totWeight = 0.0;
		this.classHierarchyDistance = new ClassHierarchyDistance(querylog.getNavigationGraph());
		this.itemMap = new HashMap<>();
		this.inverseItemMap = new HashMap<>();
		Set<Triple> nonInvertedTriples = this.simpleQuerylog.getAllNonInvertedTriples();
		Set<Triple> allTriples = this.simpleQuerylog.getAllTriples();
		Set<String> allClasses = this.simpleQuerylog.getAllClasses(); // Get all classes in nav graph, or only classes used in querylog ??
		this.itemCount = nonInvertedTriples.size() + allClasses.size();
		if(itemPropertyOnly) this.itemCount = nonInvertedTriples.size();
		this.queryCount = querylog.size();
		this.matrix =  new double[this.queryCount][this.itemCount];
		this.weights = new double[this.queryCount];
		this.createMaps(nonInvertedTriples, allTriples, allClasses);
		this.populateMatrix();
	}

	public double[][] getMatrix() {
		return this.matrix;
	}

	public double[][] getMatrixTranspose() {
		return RankerUtils.transpose(this.matrix);
	}

	public double[] getWeights() {
		return this.weights;
	}

	public double getTotWeight() {
		return this.totWeight;
	}

	public boolean getItemPropertyOnly() {
		return this.itemPropertyOnly;
	}

	public NavigationGraph getNavigationGraph() {
		return this.simpleQuerylog.getNavigationGraph();
	}

	public Map<Integer, Set<Item>> getItemMap() {
		return this.itemMap;
	}

	public Map<Item, Integer> getInverseItemMap() {
		return this.inverseItemMap;
	}

	public Set<Item> getItemsFromId(int id) {
		return itemMap.get(id);
	}

	public int getIdFromItem(Item item) {
		return this.inverseItemMap.get(item);
	}

	public int getIdFromTriple(Triple triple) {
		return this.inverseItemMap.get(new ItemProperty(triple));
	}

	public int getIdFromClass(String classLabel) {
		return this.inverseItemMap.get(new ItemClass(classLabel));
	}

	public int itemCount() {
		return this.itemCount;
	}

	public int queryCount() {
		return this.queryCount;
	}

	public boolean isObjectProperty(Triple triple) {
		return this.simpleQuerylog.isObjectProperty(triple);
	}

	public boolean isDatatypeProperty(Triple triple) {
		return this.simpleQuerylog.isDatatypeProperty(triple);
	}

	public void useInverseUserFrequency() {
		for(int i = 0; i < this.itemCount; i++) {
			double weightSum = 0.0;
			for(int j = 0; j < this.queryCount; j++) {
				weightSum += this.matrix[j][i];
			}
			double newWeight = Math.log10(this.totWeight / weightSum);
			for(int j = 0; j < this.queryCount; j++) {
				this.matrix[j][i] *= newWeight;
			}
		}
	}

	private void createMaps(Set<Triple> nonInvertedTriples, Set<Triple> allTriples, Set<String> allClasses) {
		int i = 0;

		// Non-inverse ItemProperties
		for (Triple triple : nonInvertedTriples) {
			Set<Item> tmpSet = new HashSet<>();
			Item tmpItem = new ItemProperty(triple);
			tmpSet.add(tmpItem);
			this.itemMap.put(i, tmpSet);
			this.inverseItemMap.put(tmpItem, i);
			i++;
		}

		// Inverse ItemProperties
		for (Triple inverseTriple : allTriples) {
			if(!inverseTriple.isInverted()) continue;
			for(Entry<Integer, Set<Item>> entry : this.itemMap.entrySet()) {
				int id = entry.getKey();
				ItemProperty item = (ItemProperty)entry.getValue().iterator().next();
				if(inverseTriple.inverseOf(item.getTriple())) {
					ItemProperty inverseItem = new ItemProperty(inverseTriple);
					entry.getValue().add(inverseItem);
					this.inverseItemMap.put(inverseItem, id);
				}
			}
		}

		// ItemClasses
		for (String c : allClasses) {
			Set<Item> tmpSet = new HashSet<>();
			Item tmpItem = new ItemClass(c);
			tmpSet.add(tmpItem);
			this.itemMap.put(i, tmpSet);
			this.inverseItemMap.put(tmpItem, i);
			i++;
		}

	}

	private void populateMatrix() {
		Set<SimpleQuery> queries = this.simpleQuerylog.getQueries();
		int i = 0;
		for (SimpleQuery q : queries) {
			Set<Item> itemSet = RankerUtils.simpleQueryToItemSet(q, this);
			double queryWeight = q.getWeight();
			this.weights[i] = queryWeight;
			this.totWeight += queryWeight;
			for(Item item : itemSet) {
				this.matrix[i][this.inverseItemMap.get(item)] += 1;
				this.subclassWeightDecay(item, i);
			}
			i++;
		}
	}

	private void subclassWeightDecay(Item item, int queryNumber) {
		if(!(item instanceof ItemClass)) return;
		Map<String, Integer> connectedClasses = this.classHierarchyDistance.connectedClasses(item.getLabel());
		for(Entry<String, Integer> entry : connectedClasses.entrySet()) {
			int itemId = this.getIdFromClass(entry.getKey());
			int distance = entry.getValue();
			this.matrix[queryNumber][itemId] += this.decayFactor(distance);
		}
	}

	private double decayFactor(int distance) {
		return Math.pow(this.decay, distance);
	}

	/* ONLY USED FOR FACTORIZATION RANKER
	// Return row index of new user
	public int addUser(QueryGraph qg) {
		SimpleQuery sq = RankerUtils.queryGraphToSimpleQuery(qg);
		Set<Item> itemSet = sq.getQuery();
		int queryCount= this.matrix.length;
		int itemCount = this.matrix[0].length;
		double[][] newMatrix = new double[queryCount + 1][itemCount];
		for (int i = 0; i < queryCount; i++) {
			for (int j = 0; j < itemCount; j++) {
				newMatrix[i][j] = this.matrix[i][j];
			}
		}
		for(Item tv : itemSet) {
			newMatrix[queryCount][this.inverseItemMap.get(tv)] = 1.0;
		}
		this.matrix = newMatrix;
		return queryCount;
	}
	*/

	public File createRatingsFile() {
		File tempRatingsFile = null;
		try {
			tempRatingsFile = File.createTempFile("ratingsmatrix", ".tmp");
		} catch (IOException e) {
			e.printStackTrace();
		}
		tempRatingsFile.deleteOnExit();

		PrintWriter printWriter = null;
		try {
			printWriter = new PrintWriter(new FileWriter(tempRatingsFile));
		} catch (IOException e) {
			e.printStackTrace();
		}

		int queryCount= this.matrix.length;
		int itemCount = this.matrix[0].length;
		for(int i = 0; i < queryCount; i++) {
			for(int j = 0; j < itemCount; j++) {
				if(this.matrix[i][j] != 0.0) {
					printWriter.println(i + "," + j + "," + this.matrix[i][j]);
				}
			}
		}
		printWriter.close();
		return tempRatingsFile;
	}
}
