package eu.optiquevqs.ranker.data_structures;

import java.util.List;

import eu.optiquevqs.ranker.enums.RecommendationType;

/*
 * rankerConfigs: 	List of ranker configs. See RankerConfig class.
 * splitRatio: 		Relative size of testSet compared to original query log.
 * 					(Number between 0 and 1).	
 * evalCount: 		Number of evaluations per ranker config and number of values
 * 					in minEdgeCounts.
 * minEdgeCounts:	Constraint on number of edges in queries that should be 
 * 					evaluated. For extension ranking values should be >= 2.
 * exactEdgeCounts: Only evaluates queries of with given edge count. Use -1 to ignore.
 * levelsOfResults:	1,2 or 3.
 * 					1 - Only top level results.
 * 					2 - Also results per run. (only applicable for old eval)
 * 					3 - Also results per recommend.
 * detailed:		Specifies level of detail in output files.	
 * 					false: only results
 * 					true: additional information
 */
public class EvaluationConfig {
	public List<RankerConfig> rankerConfigs;
	public double splitRatio = 0.2;
	public int evalCount = 100;
	public Integer[] minEdgeCounts = new Integer[] {2};
	public Integer[] exactEdgeCounts = new Integer[] {-1};
	public int levelsOfResults = 1;
	public boolean detailed = false;
	public boolean printProgress = false;
	public boolean onlyExtensionRecommends = true; // Only use for old eval (not Evaluation2)
	public RecommendationType recType = RecommendationType.EXTENSION;
	public boolean excludeSameEdgeQueries = false; // Queries w/ 75% of edges being the same link.
	public boolean onlySameEdgeQueries = false; // Cannot be true while excludeSameEdgeQueries is true
	public boolean excludeLongTailProperties = false; // Only evaluate for test queries where correct extension is not long tail prop
	public boolean onlyLongTailProperties = false; // Only evaluate for test queries where correct extension IS long tail prop
}
