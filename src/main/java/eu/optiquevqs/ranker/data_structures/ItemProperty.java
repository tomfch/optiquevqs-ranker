package eu.optiquevqs.ranker.data_structures;

import java.util.Objects;

import eu.optiquevqs.ranker.enums.Category;

public class ItemProperty extends Item {
	private Triple triple;

	public ItemProperty(Triple triple) {
		super(triple.getPredicate());
		this.triple = triple;
	}

	public ItemProperty(Category category, String sourceType, String edgeLabel, String targetType) {
		this(new Triple(sourceType, edgeLabel, targetType, category));
	}

	public ItemProperty(Category category, String targetType, String edgeLabel, String sourceType, boolean isInverted) {
		this(new Triple(sourceType, edgeLabel, targetType, category, isInverted));
	}

	public Triple getTriple() {
		return this.triple;
	}

	@Override
	public String toString() {
		return triple.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof ItemProperty)) return false;

		ItemProperty v = (ItemProperty)o;
		if (!v.getTriple().equals(this.triple)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.triple);
	}
}
