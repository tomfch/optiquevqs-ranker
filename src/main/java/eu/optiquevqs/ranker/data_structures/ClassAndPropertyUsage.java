package eu.optiquevqs.ranker.data_structures;

public class ClassAndPropertyUsage {
	public String type;
	public String clazz;
	public String subject;
	public String predicate;
	public String object;
	public int queryCount;
	public double weightSum;

	public ClassAndPropertyUsage(String type) {
		this.type = type;
		this.clazz = "";
		this.subject = "";
		this.predicate = "";
		this.object = "";
		this.queryCount = 0;
		this.weightSum = 0;
	}

	public void addClass(String clazz) {
		this.clazz = clazz;
	}

	public void addProperty(String predicate) {
		this.predicate = predicate;
	}

	public void addTriple(String subject, String predicate, String object) {
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
	}

	public void addInstance(double weight) {
		this.queryCount++;
		this.weightSum += weight;
	}
}
