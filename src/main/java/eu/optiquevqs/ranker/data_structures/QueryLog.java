package eu.optiquevqs.ranker.data_structures;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.graph.query.QueryGraphFactory;
import eu.optiquevqs.graph.query.QueryVariable;
import eu.optiquevqs.graph.query.QuerylogStats;

public class QueryLog {

	private NavigationGraph navigationGraph;
	private Set<QueryGraph> queries;
	private double totWeight;

	public QueryLog(NavigationGraph navigationGraph) {
		this.navigationGraph = navigationGraph;
		this.queries = new HashSet<QueryGraph>();
		this.totWeight = 0.0;
	}

	// Copy constructor
	public QueryLog(QueryLog another) {
		this(another.getNavigationGraph());
		Iterator<QueryGraph> it = another.iterator();
		while(it.hasNext()) this.addQueryGraph(it.next());
	}

	public double getTotWeight() {
		return this.totWeight;
	}

	public Set<QueryGraph> getQueries() {
		return this.queries;
	}

	public Iterator<QueryGraph> iterator() {
		return this.queries.iterator();
	}

	public NavigationGraph getNavigationGraph() {
		return this.navigationGraph;
	}

	public void addQueryGraph(QueryGraph queryGraph) {
		this.queries.add(queryGraph);
		this.totWeight += queryGraph.getWeight();
	}

	/*
	 *  Add a query log from file. File must be tsv with weights (WEIGHT QUERY).
	 *  Only adds queries that are optique compatible.
	 */
	public void addQueryLogFromFile(String filename) {
		Set<QueryGraph> queries = QueryGraphFactory.readOptiqueQueries(filename, this.navigationGraph);
		for(QueryGraph query : queries) this.addQueryGraph(query);
	}

	public void addQueryFromFile(String filename) {
		QueryGraph qg = QueryGraphFactory.read(filename, this.navigationGraph);
		qg.setWeight(1.0);
		this.addQueryGraph(qg);
	}

	public void querylogStatsToFile(String path) {
		QuerylogStats.querylogStatsToFile(this.queries, "results/");
	}

	/*
	 * Multiple uses of the same class/property/triple in will all
	 * contribute to weightSum.
	 */
	public void classAndPropertyUsageToFile(String path) {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(path + "classAndPropertyUsage.csv");
			this.header(fileWriter);
			this.classUsage(fileWriter);
			this.propertyUsage(fileWriter);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void header(FileWriter fileWriter) {
		List<String> columns = Arrays.asList(
			"type",
			"class",
			"subject",
			"predicate",
			"object",
			"queryCount",
			"weightSum"
		);

		String header = String.join(",", columns);

		try {
			fileWriter.write(header + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void classUsage(FileWriter fileWriter) {
		Map<String, ClassAndPropertyUsage> map = new HashMap<>();
		for(QueryGraph query : this.queries) {
			for(QueryVariable var : query.vertexSet()) {
				String varType = var.getType();
				if(!map.containsKey(varType)) {
					map.put(varType, new ClassAndPropertyUsage("class"));
					map.get(varType).addClass(varType);
				}
				map.get(varType).addInstance(query.getWeight());
			}
		}

		for(ClassAndPropertyUsage entry : map.values()) {
			this.classAndPropertyWrite(fileWriter, entry);
		}
	}
	private void propertyUsage(FileWriter fileWriter) {
		Map<String, ClassAndPropertyUsage> propertyMap = new HashMap<>();
		Map<String, ClassAndPropertyUsage> tripleMap = new HashMap<>();
		for(QueryGraph query : this.queries) {
			for(PropertyEdge edge : query.edgeSet()) {
				String edgeLabel = edge.getLabel();
				String sourceType = query.getEdgeSource(edge).getType();
				String targetType = query.getEdgeTarget(edge).getType();
				String tripleString = "<" + sourceType + ", " + edgeLabel + ", " + targetType + ">";

				if(!propertyMap.containsKey(edgeLabel)) {
					propertyMap.put(edgeLabel, new ClassAndPropertyUsage("property"));
					propertyMap.get(edgeLabel).addProperty(edgeLabel);
				}
				propertyMap.get(edgeLabel).addInstance(query.getWeight());

				if(!tripleMap.containsKey(tripleString)) {
					tripleMap.put(tripleString, new ClassAndPropertyUsage("triple"));
					tripleMap.get(tripleString).addTriple(sourceType, edgeLabel, targetType);
				}
				tripleMap.get(tripleString).addInstance(query.getWeight());
			}
		}

		Map<String, ClassAndPropertyUsage> combinedMap = new HashMap<>(propertyMap);
		combinedMap.putAll(tripleMap);
		for(ClassAndPropertyUsage entry : combinedMap.values()) {
			this.classAndPropertyWrite(fileWriter, entry);
		}
	}

	private void classAndPropertyWrite(FileWriter fileWriter, ClassAndPropertyUsage entry) {
		List<String> columns = Arrays.asList(
			(entry.type),
			(entry.clazz),
			(entry.subject),
			(entry.predicate),
			(entry.object),
			String.valueOf(entry.queryCount),
			String.valueOf(entry.weightSum)
		);

		String stats = String.join(",", columns);

		try {
			fileWriter.write(stats + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addQueryFromFile(String filename, double weight) {
		QueryGraph qg = QueryGraphFactory.read(filename, this.navigationGraph);
		qg.setWeight(weight);
		this.addQueryGraph(qg);
	}

	// Returns temp file with global stats for query log.
	public File globalStatsFile() {
		File file = null;
		try {
			file = File.createTempFile("temp", null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		PrintWriter printWriter = new PrintWriter(fileWriter);

		printWriter.print("queryCount");
		printWriter.print(",");
		printWriter.print("weightSum");
		printWriter.print(",");
		printWriter.print("objectPropertyWeight");
		printWriter.print(",");
		printWriter.print("datatypePropertyWeight");
		printWriter.print(",");
		printWriter.print("classesInNavGraphCount");
		printWriter.print(",");
		printWriter.print("classesInQueriesCount");
		printWriter.print(",");
		printWriter.print("objectPropertiesInNavGraphCount");
		printWriter.print(",");
		printWriter.print("objectPropertiesInQueriesCount");
		printWriter.print(",");
		printWriter.print("datatypePropertiesInNavGraphCount");
		printWriter.print(",");
		printWriter.print("datatypePropertiesInQuereisCount");
		printWriter.println();

		Set<String> classesInNavGraph = this.navigationGraph.getAllClasses();
		Set<String> classesInQueries = new HashSet<>();
		Set<String> objectPropertiesInNavGraph= new HashSet<>();
		Set<String> objectPropertiesInQueries = new HashSet<>();
		Set<String> datatypePropertiesInNavGraph= new HashSet<>();
		Set<String> datatypePropertiesInQueries = new HashSet<>();

		int queryCount = 0;
		double weightSum = 0;
		double objectPropertyWeight = 0;
		double datatypePropertyWeight = 0;

		for(QueryGraph qg : this.queries) {
			double weight = qg.getWeight();
			queryCount++;
			weightSum += qg.getWeight();
			Set<PropertyEdge> edges = qg.edgeSet();
			for(PropertyEdge edge : edges) {
				String edgeLabel = edge.getLabel();
				if(this.navigationGraph.isDatatypeProperty(edgeLabel)) {
					datatypePropertyWeight += weight / edges.size();
					datatypePropertiesInQueries.add(edgeLabel);
				} else if (this.navigationGraph.isObjectProperty(edgeLabel)) {
					objectPropertyWeight += weight / edges.size();
					objectPropertiesInQueries.add(edgeLabel);
				}
			}
			Set<QueryVariable> nodes = qg.vertexSet();
			for(QueryVariable node : nodes) {
				String nodeType = node.getType();
				if(classesInNavGraph.contains(nodeType)) {
					classesInQueries.add(nodeType);
				}
			}
			Set<PropertyEdge> edgesInNavGraph = this.navigationGraph.edgeSet();
			for(PropertyEdge edge : edgesInNavGraph) {
				String edgeLabel = edge.getLabel();
				if(this.navigationGraph.isDatatypeProperty(edgeLabel)) {
					datatypePropertiesInNavGraph.add(edgeLabel);
				} else if (this.navigationGraph.isObjectProperty(edgeLabel)) {
					objectPropertiesInNavGraph.add(edgeLabel);
				}
				
			}
		}

		int classesInNavGraphCount = classesInNavGraph.size();
		int classesInQueriesCount = classesInQueries.size();
		int objectPropertiesInNavGraphCount = objectPropertiesInNavGraph.size();
		int objectPropertiesInQueriesCount = objectPropertiesInQueries.size();
		int datatypePropertiesInNavGraphCount = datatypePropertiesInNavGraph.size();
		int datatypePropertiesInQueriesCount = datatypePropertiesInQueries.size();

		printWriter.print(queryCount);
		printWriter.print(",");
		printWriter.print(weightSum);
		printWriter.print(",");
		printWriter.print(objectPropertyWeight);
		printWriter.print(",");
		printWriter.print(datatypePropertyWeight);
		printWriter.print(",");
		printWriter.print(classesInNavGraphCount);
		printWriter.print(",");
		printWriter.print(classesInQueriesCount);
		printWriter.print(",");
		printWriter.print(objectPropertiesInNavGraphCount);
		printWriter.print(",");
		printWriter.print(objectPropertiesInQueriesCount);
		printWriter.print(",");
		printWriter.print(datatypePropertiesInNavGraphCount);
		printWriter.print(",");
		printWriter.print(datatypePropertiesInQueriesCount);
		printWriter.println();

		printWriter.close();
		file.deleteOnExit();
		return file;
	}

	// Returns temp file with query stats for query log. One row per query.
	public File queryStatsFile() {
		File file = null;
		try {
			file = File.createTempFile("temp", null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		PrintWriter printWriter = new PrintWriter(fileWriter);

		printWriter.println("weight, edgeCount, objectPropertyCount, datatypePropertyCount");

		for(QueryGraph qg : this.queries) {
			NavigationGraph ng = qg.getNavigationGraph();
			int objectPropertyCount = 0;
			int datatypePropertyCount = 0;
			Set<PropertyEdge> edges = qg.edgeSet();
			for(PropertyEdge edge : edges) {
				if (ng.isDatatypeProperty(edge.getLabel())) {
					datatypePropertyCount++;
				} else if (ng.isObjectProperty(edge.getLabel())) {
					objectPropertyCount++;
				}
			}
			double weight = qg.getWeight();
			int edgeCount = edges.size();
			printWriter.print(weight);
			printWriter.print(",");
			printWriter.print(edgeCount);
			printWriter.print(",");
			printWriter.print(objectPropertyCount);
			printWriter.print(",");
			printWriter.print(datatypePropertyCount);
			printWriter.println();
		}
		printWriter.close();
		file.deleteOnExit();
		return file;
	}
}
