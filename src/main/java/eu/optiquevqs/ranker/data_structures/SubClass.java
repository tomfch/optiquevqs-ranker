package eu.optiquevqs.ranker.data_structures;

public class SubClass implements Comparable<SubClass> {
	private String classLabel;
	private double score;

	public SubClass(String classLabel) {
		this.classLabel = classLabel;
		this.score = 0.0;
	}

	public String getClassLabel() {
		return this.classLabel;
	}

	public double getScore() {
		return this.score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	@Override
	public int compareTo(SubClass subClass) {
		return Double.compare(this.score, subClass.getScore());
	}

	@Override
	public String toString() {
		return this.classLabel + "    " + this.score;
	}
}
