package eu.optiquevqs.ranker.data_structures;

import java.util.Objects;

import eu.optiquevqs.ranker.enums.Category;

public class Triple {
	private String subject;
	private String predicate;
	private String object;
	private Category category;
	private Triple topTriple; // if subject/object is a subclass, the triple with "original" classes as subject/object can be given here
	private boolean isInverted; // inverse predicate

	public Triple(String subject, String predicate, String object, Category category) {
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
		this.category = category;
		this.topTriple = null;
		this.isInverted = false;
	}

	public Triple(String subject, String predicate, String object, Category category, boolean isInverted) {
		this(subject, predicate, object, category);
		this.isInverted = isInverted;
	}

	public Triple(String subject, String predicate, String object, Category category, Triple topTriple) {
		this(subject, predicate, object, category);
		this.topTriple = topTriple;
	}

	public Triple(String subject, String predicate, String object, Category category, boolean isInverted, Triple topTriple) {
		this(subject, predicate, object, category, topTriple);
		this.isInverted = isInverted;
	}

	public Triple getNonInvertedTriple() {
		if(this.isInverted) return new Triple(this.object, this.predicate, this.subject, this.category);
		return this;
	}

	public String getSubject() {
		return this.subject;
	}

	public String getPredicate() {
		return this.predicate;
	}

	public String getObject() {
		return this.object;
	}

	public Triple getTopTriple() {
		return this.topTriple;
	}

	public Category getCategory() {
		return this.category;
	}

	public boolean isInverted() {
		return this.isInverted;
	}

	public boolean inverseOf(Triple another) {
		if(!this.subject.equals(another.getObject())) return false;
		if(!this.predicate.equals(another.getPredicate())) return false;
		if(!this.object.equals(another.getSubject())) return false;
		if(!(this.isInverted ^ another.isInverted())) return false;

		return true;
	}

	@Override
	public String toString() {
		String triple = "<" + this.subject + ", " + this.predicate + ", " + this.object + ">";
		String inverse = this.isInverted ? " -1" : "";
		return triple + inverse;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Triple)) return false;

		Triple v = (Triple)o;
		if (!v.getSubject().equals(this.subject)) return false;
		if (!v.getPredicate().equals(this.predicate)) return false;
		if (!v.getObject().equals(this.object)) return false;
		if (!v.isInverted() == (this.isInverted)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.subject, this.predicate, this.object, this.isInverted);
	}

}
