package eu.optiquevqs.ranker.enums;

public enum RankerType {
	ITEM,
	RULES,
	MOSTPOPULAR,
	RANDOM,
	FACTORIZATION,
}
