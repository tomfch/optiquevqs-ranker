package eu.optiquevqs.ranker.enums;

// Category for extension recommendations.
public enum Category {
	OBJECT,
	DATATYPE,
}
