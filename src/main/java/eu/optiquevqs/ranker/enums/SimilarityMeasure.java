package eu.optiquevqs.ranker.enums;

// Note that all similarity measure are modified to work with weighted queries.
public enum SimilarityMeasure {
	COSINE,
	EUCLIDIAN,
	JACCARD_OLD,
	RUZICKA,
	JACCARD,
	DICE,
}
