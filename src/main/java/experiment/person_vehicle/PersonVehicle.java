package experiment.person_vehicle;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.SubClassEdge;
import eu.optiquevqs.graph.navigation.ClassNode;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.navigation.NavigationGraphNode;
import eu.optiquevqs.ranker.data_structures.QueryLog;

public class PersonVehicle {
	public static void main(String[] args) {
		NavigationGraph ng = createNavigationGraph();
		QueryLog ql = new QueryLog(ng);
		addQueries(ql);

		System.out.println("DONE!");
	}

	static void addQueries(QueryLog ql) {
		ql.addQueryFromFile("src/main/resources/personvehicle/q01.rq", 1.0);
		ql.addQueryFromFile("src/main/resources/personvehicle/q02.rq", 1.0);
		ql.addQueryFromFile("src/main/resources/personvehicle/q03.rq", 1.0);
		ql.addQueryFromFile("src/main/resources/personvehicle/q04.rq", 1.0);
	}

	static NavigationGraph createNavigationGraph() {
		NavigationGraph ng = new NavigationGraph();

		NavigationGraphNode cPerson = new ClassNode("http://www.example.org/Person"); 
		NavigationGraphNode cAdult= new ClassNode("http://www.example.org/Adult"); 
		NavigationGraphNode cChild= new ClassNode("http://www.example.org/Child"); 
		NavigationGraphNode cVehicle= new ClassNode("http://www.example.org/Vehicle"); 
		NavigationGraphNode cCar= new ClassNode("http://www.example.org/Car"); 
		NavigationGraphNode cBicycle= new ClassNode("http://www.example.org/Bicycle"); 

		ng.addVertex(cPerson);
		ng.addVertex(cAdult);
		ng.addVertex(cChild);
		ng.addVertex(cVehicle);
		ng.addVertex(cCar);
		ng.addVertex(cBicycle);

		String knows = "http://www.example.org/knows"; 
		String likes = "http://www.example.org/likes"; 
		String owns = "http://www.example.org/owns"; 

		ng.addEdge(cPerson, cPerson, new PropertyEdge(knows));
		ng.addEdge(cPerson, cPerson, new PropertyEdge(likes));
		ng.addEdge(cPerson, cVehicle, new PropertyEdge(owns));

		ng.addEdge(cAdult, cPerson, new SubClassEdge());
		ng.addEdge(cChild, cPerson, new SubClassEdge());
		ng.addEdge(cCar, cVehicle, new SubClassEdge());
		ng.addEdge(cBicycle, cVehicle, new SubClassEdge());

		return ng;
	}

}
