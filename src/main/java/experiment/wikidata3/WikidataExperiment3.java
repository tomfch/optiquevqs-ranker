package experiment.wikidata3;

import java.util.ArrayList;
import java.util.List;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.SubClassEdge;
import eu.optiquevqs.graph.navigation.ClassNode;
import eu.optiquevqs.graph.navigation.DatatypeNode;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.ranker.data_structures.EvaluationConfig;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.enums.RankerType;
import eu.optiquevqs.ranker.enums.SimilarityMeasure;
import evaluation.Evaluation;

public class WikidataExperiment3 {
	public static void main(String[] args) {
		// Create query log
		NavigationGraph ng = createNavigationGraph();
		QueryLog ql = new QueryLog(ng);
		addQueries(ql);
		System.out.println("Query log loaded");

		// Evaluation parameters
		/*
		List<RankerConfig> rankerConfigs = new ArrayList<>();
		List<SimilarityMeasure> simMeasures = new ArrayList<>();
		simMeasures.add(SimilarityMeasure.COSINE);
		simMeasures.add(SimilarityMeasure.EUCLIDIAN);
		simMeasures.add(SimilarityMeasure.RUZICKA);
		simMeasures.add(SimilarityMeasure.JACCARD_OLD);
		simMeasures.add(SimilarityMeasure.JACCARD);
		for(SimilarityMeasure simMeasure : simMeasures) {
			RankerConfig rankerConfig = new RankerConfig();
			rankerConfig.rankerType = RankerType.ITEM;
			rankerConfig.similarityMeasure = simMeasure;
			rankerConfigs.add(rankerConfig);
		}
		*/
		List<RankerConfig> rankerConfigs = new ArrayList<>();
		RankerConfig rc1 = new RankerConfig();
		rc1.rankerType = RankerType.ITEM;
		rc1.similarityMeasure = SimilarityMeasure.RUZICKA;
		rc1.decay = 0.2;

		RankerConfig rc11 = new RankerConfig();
		rc11.rankerType = RankerType.ITEM;
		rc11.similarityMeasure = SimilarityMeasure.EUCLIDIAN;
		rc11.decay = 0.2;

		RankerConfig rc12 = new RankerConfig();
		rc12.rankerType = RankerType.ITEM;
		rc12.similarityMeasure = SimilarityMeasure.RUZICKA;
		rc12.decay = 0.2;

		RankerConfig rc13 = new RankerConfig();
		rc13.rankerType = RankerType.ITEM;
		rc13.similarityMeasure = SimilarityMeasure.JACCARD_OLD;
		rc13.decay = 0.2;

		RankerConfig rc14 = new RankerConfig();
		rc14.rankerType = RankerType.ITEM;
		rc14.similarityMeasure = SimilarityMeasure.JACCARD;
		rc14.decay = 0.2;

		RankerConfig rc2 = new RankerConfig();
		rc2.rankerType = RankerType.RULES;
		rc2.minSupport = 10;
		rc2.minConfidence = 0.1;
		rc2.decay = 0.2;

		RankerConfig rc3 = new RankerConfig();
		rc3.rankerType = RankerType.RANDOM;
		rc3.decay = 0.2;

		RankerConfig rc4 = new RankerConfig();
		rc4.rankerType = RankerType.MOSTPOPULAR;
		rc4.decay = 0.2;

		rankerConfigs.add(rc1);
		rankerConfigs.add(rc11);
		rankerConfigs.add(rc12);
		rankerConfigs.add(rc13);
		rankerConfigs.add(rc14);
		//rankerConfigs.add(rc2);
		rankerConfigs.add(rc3);
		rankerConfigs.add(rc4);

		EvaluationConfig evaluationConfig = new EvaluationConfig();
		evaluationConfig.rankerConfigs = rankerConfigs;
		evaluationConfig.splitRatio = 0.2;
		evaluationConfig.evalCount = 1000;
		evaluationConfig.minEdgeCounts = new Integer[] {2};
		evaluationConfig.levelsOfResults = 3;
		evaluationConfig.detailed = false;
		evaluationConfig.printProgress = true;

		// Run evaluation
		Evaluation evaluation = new Evaluation(ql, "results/");
		evaluation.evaluate(evaluationConfig);

		System.out.println("Done!");
	}

	public static NavigationGraph createNavigationGraph() {
        // The wikidata navigation graph
        NavigationGraph navigationGraph = new NavigationGraph();

        // Define class URIs
        String classActorUri            = "http://www.wikidata.org/entity/Q33999";
        String classAwardUri            = "http://www.wikidata.org/entity/Q618779";
        String classBookUri             = "http://www.wikidata.org/entity/Q571";
        String classCapitalUri          = "http://www.wikidata.org/entity/Q5119";
        String classCityUri             = "http://www.wikidata.org/entity/Q515";
        String classCityUSAUri          = "http://www.wikidata.org/entity/Q1093829";
        String classContinentUri        = "http://www.wikidata.org/entity/Q5107";
        String classCountryUri          = "http://www.wikidata.org/entity/Q6256";
        String classDictionaryEntryUri  = "http://www.wikidata.org/entity/Q4423781";
        String classEyeColorUri         = "http://www.wikidata.org/entity/Q23786";
        String classFemaleUri           = "http://www.wikidata.org/entity/Q6581072";
        String classFilmDirectorUri     = "http://www.wikidata.org/entity/Q2526255";
        String classFilmGenreUri        = "http://www.wikidata.org/entity/Q201658";
        String classFilmographyUri      = "http://www.wikidata.org/entity/Q1371849";
        String classFilmUri             = "http://www.wikidata.org/entity/Q11424";
        String classGenderUri           = "http://www.wikidata.org/entity/Q48277";
        String classHouseCatUri         = "http://www.wikidata.org/entity/Q146";
        String classHumanHairColorUri   = "http://www.wikidata.org/entity/Q1048314";
        String classHumanUri            = "http://www.wikidata.org/entity/Q5";
        String classMaleUri             = "http://www.wikidata.org/entity/Q6581097";
        String classMunicipalityUri     = "http://www.wikidata.org/entity/Q15284";
        String classPaintingUri         = "http://www.wikidata.org/entity/Q3305213";
        String classProfessionUri       = "http://www.wikidata.org/entity/Q28640";
        String classTelevisionSeriesUri = "http://www.wikidata.org/entity/Q5398426";

        String classPersonUri			= "http://www.wikidata.org/entity/Q215627";
        String classComposerUri			= "http://www.wikidata.org/entity/Q36834";
        String classArtistUri			= "http://www.wikidata.org/entity/Q483501";
        String classOrganizationUri		= "http://www.wikidata.org/entity/Q43229";
        String classMusicalGroupUri		= "http://www.wikidata.org/entity/Q215380";
        String classRockBandUri			= "http://www.wikidata.org/entity/Q5741069";
        String classWorkUri				= "http://www.wikidata.org/entity/Q386724";
        String classWorkOfArtUri		= "http://www.wikidata.org/entity/Q838948";
        String classSingleUri			= "http://www.wikidata.org/entity/Q134556";
        String classAlbumUri			= "http://www.wikidata.org/entity/Q482994";
        String classSongUri				= "http://www.wikidata.org/entity/Q7366";
        String classMusicGenreUri		= "http://www.wikidata.org/entity/Q188451";
        String classEducationalInstitutionUri = "http://www.wikidata.org/entity/Q2385804";

        // Define datatype URIs
        String datatypeStringUri   = "String";
        String datatypeIntegerUri  = "Integer";
        String datatypeDoubleUri   = "Double";
        String datatypeDatetimeUri = "Datetime";
        String datatypeLocationUri = "Location";

        // Define property URIs
        String aboutUri                 = "http://schema.org/about";
        String awardReceivedUri         = "http://www.wikidata.org/prop/direct/P166";
        String birthNameUri             = "http://www.wikidata.org/prop/direct/P1477";
        String capitalUri               = "http://www.wikidata.org/prop/direct/P36";
        String castMemberUri            = "http://www.wikidata.org/prop/direct/P161";
        String childUri                 = "http://www.wikidata.org/prop/direct/P40";
        String coordinateLocationUri    = "http://www.wikidata.org/prop/direct/P625";
        String countryOfCitzenshipUri   = "http://www.wikidata.org/prop/direct/P27";
        String countryOfOriginUri       = "http://www.wikidata.org/prop/direct/P495";
        String creatorUri               = "http://www.wikidata.org/prop/direct/P170";
        String dateOfBirthUri           = "http://www.wikidata.org/prop/direct/P569";
        String descriptionUri           = "http://schema.org/description";
        String diplomaticRelationUri    = "http://www.wikidata.org/prop/direct/P530";
        String directorUri              = "http://www.wikidata.org/prop/direct/P57";
        String endTimeUri               = "http://www.wikidata.org/prop/direct/P582";
        String eyeColorUri              = "http://www.wikidata.org/prop/direct/P1340";
        String familyNameUri            = "http://www.wikidata.org/prop/direct/P734";
        String filmingLocationUri       = "http://www.wikidata.org/prop/direct/P915";
        String filmographyUri           = "http://www.wikidata.org/prop/direct/P1283";
        String genreUri                 = "http://www.wikidata.org/prop/direct/P136";
        String givenNameUri             = "http://www.wikidata.org/prop/direct/P735";
        String hairColorUri             = "http://www.wikidata.org/prop/direct/P1884";
        String headOfGovernmentUri      = "http://www.wikidata.org/prop/direct/P6";
        String headOfStateUri           = "http://www.wikidata.org/prop/direct/P35";
        String heightUri                = "http://www.wikidata.org/prop/direct/P2048";
        String inceptionUri             = "http://www.wikidata.org/prop/direct/P571";
        String labelUri                 = "http://www.w3.org/2000/01/rdf-schema#label";
        String lifeExpectancyUri        = "http://www.wikidata.org/prop/direct/P2250";
        String locatedInUri             = "http://www.wikidata.org/prop/direct/P131";
        String mediaTitleUri            = "http://www.wikidata.org/prop/direct/P1476";
        String nameNativeLanguageUri    = "http://www.wikidata.org/prop/direct/P1559";
        String nameUri                  = "http://www.wikidata.org/prop/direct/P2561";
        String numberOfChildrenUri      = "http://www.wikidata.org/prop/direct/P1971";
        String occupationUri            = "http://www.wikidata.org/prop/direct/P106";
        String officialNameUri          = "http://www.wikidata.org/prop/direct/P1448";
        String partOfUri                = "http://www.wikidata.org/prop/direct/P361";
        String placeOfBirthUri          = "http://www.wikidata.org/prop/direct/P19";
        String populationUri            = "http://www.wikidata.org/prop/direct/P1082";
        String residenceUri             = "http://www.wikidata.org/prop/direct/P551";
        String screenWriterUri          = "http://www.wikidata.org/prop/direct/P58";
        String sexOrGenderUri           = "http://www.wikidata.org/prop/direct/P21";
        String sharesBorderWithUri      = "http://www.wikidata.org/prop/direct/P47";
        String siblingUri               = "http://www.wikidata.org/prop/direct/P3373";
        String spouseUri                = "http://www.wikidata.org/prop/direct/P26";
        String startTimeUri             = "http://www.wikidata.org/prop/direct/P580";
        String studentOfUri             = "http://www.wikidata.org/prop/direct/P1066";
        String workLocationUri          = "http://www.wikidata.org/prop/direct/P937";

        String performerUri = "http://www.wikidata.org/prop/direct/P175";
        String publicationDateUri = "http://www.wikidata.org/prop/direct/P577";
        //String partOfUri = "http://www.wikidata.org/prop/direct/P361";
        String OCLCcontrolNumberUri = "http://www.wikidata.org/prop/direct/P243";
        //String publicationDateUri = "http://www.wikidata.org/prop/direct/P577";
        String authorUri = "http://www.wikidata.org/prop/direct/P50";
        String musicBrainzReleaseGroupIdUri = "http://www.wikidata.org/prop/direct/P436";
        String musicBrainzArtistIdUri = "http://www.wikidata.org/prop/direct/P434";
        //String genreUri = "http://www.wikidata.org/prop/direct/P136";
        String durationUri = "http://www.wikidata.org/prop/direct/P2047";
        String locationUri = "http://www.wikidata.org/prop/direct/P279";
        String titleUri = "http://www.wikidata.org/prop/direct/P1476";
        //String creatorUri = "http://www.wikidata.org/prop/direct/P170";
        String countryUri = "http://www.wikidata.org/prop/direct/P17";
        //String countryOfOriginUri = "http://www.wikidata.org/prop/direct/P495";
        String IMBDidUri = "http://www.wikidata.org/prop/direct/P345";
        String songKickArtistIdUri = "http://www.wikidata.org/prop/direct/P3478";
        String nicknameUri = "http://www.wikidata.org/prop/direct/P1449";
        String officialWebsiteUri = "http://www.wikidata.org/prop/direct/P856";
        //String inceptionUri = "http://www.wikidata.org/prop/direct/P571";
        String spotifyArtistIdUri = "http://www.wikidata.org/prop/direct/P1902";
        String headquartersLocationUri = "http://www.wikidata.org/prop/direct/P159";
        String dissolvedUri = "http://www.wikidata.org/prop/direct/P576";
        String foundedByUri = "http://www.wikidata.org/prop/direct/P112";
        String causeOfDeathUri = "http://www.wikidata.org/prop/direct/P509";
        String dateOfDeathUri = "http://www.wikidata.org/prop/direct/P570";
        //String dateOfBirthUri = "http://www.wikidata.org/prop/direct/P569";
        //String sexOrGenderUri = "http://www.wikidata.org/prop/direct/P21";
        //String countryOfCitzenshipUri = "http://www.wikidata.org/prop/direct/P27";
        String VIAPidUri = "http://www.wikidata.org/prop/direct/P214";
        String freebaseIdUri = "http://www.wikidata.org/prop/direct/P646";
        String educatedAtUri = "http://www.wikidata.org/prop/direct/P69";
        String allMoviePersonIdUri = "http://www.wikidata.org/prop/direct/P2019";

        // Define classes
        ClassNode classPerson = new ClassNode(classPersonUri);
        ClassNode classArtist = new ClassNode(classArtistUri);
        ClassNode classComposer = new ClassNode(classComposerUri);
        ClassNode classOrganization = new ClassNode(classOrganizationUri);
        ClassNode classMusicalGroup = new ClassNode(classMusicalGroupUri);
        ClassNode classRockBand = new ClassNode(classRockBandUri);
        ClassNode classWork = new ClassNode(classWorkUri);
        ClassNode classWorkOfArt = new ClassNode(classWorkOfArtUri);
        ClassNode classSingle = new ClassNode(classSingleUri);
        ClassNode classAlbum = new ClassNode(classAlbumUri);
        ClassNode classSong = new ClassNode(classSongUri);
        ClassNode classMusicGenre = new ClassNode(classMusicGenreUri);
        ClassNode classCountry= new ClassNode(classCountryUri);
        ClassNode classGender = new ClassNode(classGenderUri);
        ClassNode classEducationalInstitution = new ClassNode(classEducationalInstitutionUri);

        // Define datatypes
        DatatypeNode datatypeString   = new DatatypeNode(datatypeStringUri);
        DatatypeNode datatypeInteger  = new DatatypeNode(datatypeIntegerUri);
        DatatypeNode datatypeDouble   = new DatatypeNode(datatypeDoubleUri);
        DatatypeNode datatypeDatetime = new DatatypeNode(datatypeDatetimeUri);
        DatatypeNode datatypeLocation = new DatatypeNode(datatypeLocationUri);

        // Add classes
        navigationGraph.addVertex(classPerson);
        navigationGraph.addVertex(classArtist);
        navigationGraph.addVertex(classComposer);
        navigationGraph.addVertex(classOrganization);
        navigationGraph.addVertex(classMusicalGroup);
        navigationGraph.addVertex(classRockBand);
        navigationGraph.addVertex(classWork);
        navigationGraph.addVertex(classWorkOfArt);
        navigationGraph.addVertex(classSingle);
        navigationGraph.addVertex(classAlbum);
        navigationGraph.addVertex(classSong);
        navigationGraph.addVertex(classMusicGenre);
        navigationGraph.addVertex(classCountry);
        navigationGraph.addVertex(classGender);
        navigationGraph.addVertex(classEducationalInstitution);

        // Add datatypes
        navigationGraph.addVertex(datatypeString);
        navigationGraph.addVertex(datatypeInteger);
        navigationGraph.addVertex(datatypeDouble);
        navigationGraph.addVertex(datatypeDatetime);
        navigationGraph.addVertex(datatypeLocation);

        // Properties
        navigationGraph.addEdge(classWork, datatypeString, new PropertyEdge(titleUri));
        navigationGraph.addEdge(classWork, datatypeDatetime, new PropertyEdge(publicationDateUri));
        navigationGraph.addEdge(classWork, datatypeString, new PropertyEdge(OCLCcontrolNumberUri));
        navigationGraph.addEdge(classWork, datatypeLocation, new PropertyEdge(locationUri));
        navigationGraph.addEdge(classWork, datatypeInteger, new PropertyEdge(durationUri));
        //navigationGraph.addEdge(classWorkOfArt, datatypeDatetime, new PropertyEdge(publicationDateUri)); // Moved up to Work
        navigationGraph.addEdge(classWorkOfArt, classMusicalGroup, new PropertyEdge(performerUri));
        navigationGraph.addEdge(classWorkOfArt, classPerson, new PropertyEdge(authorUri));
        //navigationGraph.addEdge(classWorkOfArt, datatypeString, new PropertyEdge(OCLCcontrolNumberUri)); // Moved up to Work
        navigationGraph.addEdge(classWorkOfArt, datatypeString, new PropertyEdge(musicBrainzReleaseGroupIdUri));
        navigationGraph.addEdge(classWorkOfArt, classMusicGenre, new PropertyEdge(genreUri));
        //navigationGraph.addEdge(classWorkOfArt, datatypeInteger, new PropertyEdge(durationUri)); // Moved up to Work
        //navigationGraph.addEdge(classWorkOfArt, datatypeLocation, new PropertyEdge(locationUri)); // Moved up to Work
        navigationGraph.addEdge(classWorkOfArt, classPerson, new PropertyEdge(creatorUri));
        navigationGraph.addEdge(classWorkOfArt, classCountry, new PropertyEdge(countryUri));
        navigationGraph.addEdge(classWorkOfArt, classCountry, new PropertyEdge(countryOfOriginUri));
        navigationGraph.addEdge(classSong, classAlbum, new PropertyEdge(partOfUri));
        navigationGraph.addEdge(classSingle, classAlbum, new PropertyEdge(partOfUri));

        navigationGraph.addEdge(classOrganization, datatypeLocation, new PropertyEdge(headquartersLocationUri));
        navigationGraph.addEdge(classOrganization, classCountry, new PropertyEdge(countryUri));
        navigationGraph.addEdge(classOrganization, datatypeDatetime, new PropertyEdge(dissolvedUri));
        navigationGraph.addEdge(classOrganization, classPerson, new PropertyEdge(foundedByUri));
        navigationGraph.addEdge(classOrganization, datatypeDatetime, new PropertyEdge(inceptionUri));
        navigationGraph.addEdge(classOrganization, datatypeString, new PropertyEdge(officialWebsiteUri));
        navigationGraph.addEdge(classOrganization, datatypeString, new PropertyEdge(nicknameUri));
        navigationGraph.addEdge(classOrganization, datatypeString, new PropertyEdge(musicBrainzArtistIdUri)); // Moved up to Organization
        //navigationGraph.addEdge(classMusicalGroup, datatypeString, new PropertyEdge(nicknameUri)); // Moved up to Organization
        //navigationGraph.addEdge(classMusicalGroup, datatypeString, new PropertyEdge(officialWebsiteUri)); // Moved up to Organization
        navigationGraph.addEdge(classMusicalGroup, datatypeString, new PropertyEdge(songKickArtistIdUri));
        navigationGraph.addEdge(classMusicalGroup, classMusicGenre, new PropertyEdge(genreUri));
        //navigationGraph.addEdge(classMusicalGroup, datatypeString, new PropertyEdge(musicBrainzArtistIdUri)); // Moved up to Organization
        //navigationGraph.addEdge(classMusicalGroup, datatypeDatetime, new PropertyEdge(inceptionUri)); // Moved up to Organization
        navigationGraph.addEdge(classMusicalGroup, datatypeString, new PropertyEdge(spotifyArtistIdUri));

        navigationGraph.addEdge(classPerson, datatypeString, new PropertyEdge(freebaseIdUri));
        navigationGraph.addEdge(classPerson, datatypeString, new PropertyEdge(allMoviePersonIdUri));
        navigationGraph.addEdge(classPerson, classEducationalInstitution, new PropertyEdge(educatedAtUri));
        navigationGraph.addEdge(classPerson, datatypeString, new PropertyEdge(causeOfDeathUri));
        navigationGraph.addEdge(classPerson, datatypeDatetime, new PropertyEdge(dateOfDeathUri));
        navigationGraph.addEdge(classPerson, datatypeDatetime, new PropertyEdge(dateOfBirthUri));
        navigationGraph.addEdge(classPerson, classGender, new PropertyEdge(sexOrGenderUri));
        navigationGraph.addEdge(classPerson, classCountry, new PropertyEdge(countryOfCitzenshipUri));
        navigationGraph.addEdge(classPerson, datatypeString, new PropertyEdge(VIAPidUri));
        //navigationGraph.addEdge(classComposer, datatypeString, new PropertyEdge(VIAPidUri)); // Moved up to Person

        // Subclasses
        navigationGraph.addEdge(classComposer, classArtist, new SubClassEdge());
        navigationGraph.addEdge(classArtist, classPerson, new SubClassEdge());
        navigationGraph.addEdge(classRockBand, classMusicalGroup, new SubClassEdge());
        navigationGraph.addEdge(classMusicalGroup, classOrganization, new SubClassEdge());
        navigationGraph.addEdge(classSingle, classWorkOfArt, new SubClassEdge());
        navigationGraph.addEdge(classAlbum, classWorkOfArt, new SubClassEdge());
        navigationGraph.addEdge(classSong, classWorkOfArt, new SubClassEdge());
        navigationGraph.addEdge(classWorkOfArt, classWork, new SubClassEdge());


        return navigationGraph;
	}

	public static void addQueries(QueryLog ql) {
		String path = "src/main/resources/wikidata3/";
		String[] files = {
			path + "2017-06-12_2017-07-09.txt",
			path + "2017-07-10_2017-08-06.txt",
			path + "2017-08-07_2017-09-03.txt",
			path + "2017-12-03_2017-12-30.txt",
			path + "2018-01-01_2018-01-28.txt",
			path + "2018-01-29_2018-02-25.txt",
			path + "2018-02-26_2018-03-25.txt"
		};
		for (String file : files) ql.addQueryLogFromFile(file);
	}
}
