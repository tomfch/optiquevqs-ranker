package experiment.wikidata;

import java.util.ArrayList;
import java.util.List;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.navigation.ClassNode;
import eu.optiquevqs.graph.navigation.DatatypeNode;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.navigation.NavigationGraphNode;
import eu.optiquevqs.ranker.data_structures.EvaluationConfig;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.enums.RankerType;
import eu.optiquevqs.ranker.enums.SimilarityMeasure;
import evaluation.Evaluation;

public class WikidataExperiment {
	public static void main(String[] args) {
		// Create query log
		NavigationGraph ng = createNavigationGraph();
		QueryLog ql = new QueryLog(ng);
		addQueries(ql);

		// Evaluation parameters
		List<RankerConfig> rankerConfigs = new ArrayList<>();
		List<SimilarityMeasure> simMeasures = new ArrayList<>();
		simMeasures.add(SimilarityMeasure.COSINE);
		simMeasures.add(SimilarityMeasure.EUCLIDIAN);
		simMeasures.add(SimilarityMeasure.RUZICKA);
		simMeasures.add(SimilarityMeasure.JACCARD_OLD);
		simMeasures.add(SimilarityMeasure.JACCARD);
		for(SimilarityMeasure simMeasure : simMeasures) {
			RankerConfig rankerConfig = new RankerConfig();
			rankerConfig.rankerType = RankerType.ITEM;
			rankerConfig.similarityMeasure = simMeasure;
			rankerConfigs.add(rankerConfig);
		}

		EvaluationConfig evaluationConfig = new EvaluationConfig();
		evaluationConfig.rankerConfigs = rankerConfigs;
		evaluationConfig.splitRatio = 0.2;
		evaluationConfig.evalCount = 10;
		evaluationConfig.minEdgeCounts = new Integer[] {2};
		evaluationConfig.levelsOfResults = 3;
		evaluationConfig.detailed = true;

		// Run evaluation
		Evaluation evaluation = new Evaluation(ql, "results/");
		evaluation.evaluate(evaluationConfig);

		System.out.println("Done!");
	}

	static NavigationGraph createNavigationGraph() {
        // The wikidata navigation graph
        NavigationGraph navigationGraph = new NavigationGraph();

        // Define classes
        NavigationGraphNode classHuman = new ClassNode("http://www.wikidata.org/entity/Q5");
        NavigationGraphNode classFilm = new ClassNode("http://www.wikidata.org/entity/Q11424");
        NavigationGraphNode classCountry = new ClassNode("http://www.wikidata.org/entity/Q6256");
        NavigationGraphNode classActor = new ClassNode("http://www.wikidata.org/entity/Q33999");
        NavigationGraphNode classFilmDirector = new ClassNode("http://www.wikidata.org/entity/Q2526255");
        NavigationGraphNode classMale = new ClassNode("http://www.wikidata.org/entity/Q6581097");
        NavigationGraphNode classFemale = new ClassNode("http://www.wikidata.org/entity/Q6581072");
        NavigationGraphNode classTelevisionSeries = new ClassNode("http://www.wikidata.org/entity/Q5398426");
        NavigationGraphNode classCity = new ClassNode("http://www.wikidata.org/entity/Q515");
        NavigationGraphNode classCapital = new ClassNode("http://www.wikidata.org/entity/Q5119");
        NavigationGraphNode classDictionaryEntry = new ClassNode("http://www.wikidata.org/entity/Q4423781");
        NavigationGraphNode classHouseCat = new ClassNode("http://www.wikidata.org/entity/Q146");
        NavigationGraphNode classPainting = new ClassNode("http://www.wikidata.org/entity/Q3305213");
        NavigationGraphNode classMunicipality = new ClassNode("http://www.wikidata.org/entity/Q15284");
        NavigationGraphNode classBook = new ClassNode("http://www.wikidata.org/entity/Q571");
        NavigationGraphNode classGender = new ClassNode("http://www.wikidata.org/entity/Q48277");
        NavigationGraphNode classProfession = new ClassNode("http://www.wikidata.org/entity/Q28640");
        NavigationGraphNode classCityUSA = new ClassNode("http://www.wikidata.org/entity/Q1093829");
        NavigationGraphNode classEyeColor = new ClassNode("http://www.wikidata.org/entity/Q23786");
        NavigationGraphNode classFilmography = new ClassNode("http://www.wikidata.org/entity/Q1371849");


        // Add classes
        navigationGraph.addVertex(classHuman);
        navigationGraph.addVertex(classFilm);
        navigationGraph.addVertex(classCountry);
        navigationGraph.addVertex(classActor);
        navigationGraph.addVertex(classFilmDirector);
        navigationGraph.addVertex(classMale);
        navigationGraph.addVertex(classFemale);
        navigationGraph.addVertex(classTelevisionSeries);
        navigationGraph.addVertex(classCity);
        navigationGraph.addVertex(classFilmography);
        navigationGraph.addVertex(classCapital);
        navigationGraph.addVertex(classDictionaryEntry);
        navigationGraph.addVertex(classHouseCat);
        navigationGraph.addVertex(classPainting);
        navigationGraph.addVertex(classMunicipality);
        navigationGraph.addVertex(classBook);
        navigationGraph.addVertex(classGender);
        navigationGraph.addVertex(classProfession);
        navigationGraph.addVertex(classCityUSA);
        navigationGraph.addVertex(classEyeColor);
        navigationGraph.addVertex(classFilmography);


        // Define datatypes
        NavigationGraphNode datatypeString = new DatatypeNode("String");
        NavigationGraphNode datatypeInteger = new DatatypeNode("Integer");
        NavigationGraphNode datatypeDouble = new DatatypeNode("Double");
        NavigationGraphNode datatypeDatetime = new DatatypeNode("Datetime");

        // Add datatypes
        navigationGraph.addVertex(datatypeString);
        navigationGraph.addVertex(datatypeInteger);
        navigationGraph.addVertex(datatypeDouble);
        navigationGraph.addVertex(datatypeDatetime);



        // Define edges
        PropertyEdge birthName             = new PropertyEdge("http://www.wikidata.org/prop/direct/P1477");
        PropertyEdge givenName             = new PropertyEdge("http://www.wikidata.org/prop/direct/P735");
        PropertyEdge familyName            = new PropertyEdge("http://www.wikidata.org/prop/direct/P734");
        PropertyEdge nameNativeLanguage    = new PropertyEdge("http://www.wikidata.org/prop/direct/P1559");
        PropertyEdge numberOfChildren      = new PropertyEdge("http://www.wikidata.org/prop/direct/P1971");
        PropertyEdge sexOrGender           = new PropertyEdge("http://www.wikidata.org/prop/direct/P21");
        //PropertyEdge sexOrGenderInv        = new PropertyEdge("http://www.wikidata.org/prop/direct/P21", true);
        PropertyEdge child                 = new PropertyEdge("http://www.wikidata.org/prop/direct/P40");
        //PropertyEdge childInv              = new PropertyEdge("http://www.wikidata.org/prop/direct/P40", true);
        PropertyEdge spouse                = new PropertyEdge("http://www.wikidata.org/prop/direct/P26");
        //PropertyEdge spouseInv             = new PropertyEdge("http://www.wikidata.org/prop/direct/P26", true);
        PropertyEdge sibling               = new PropertyEdge("http://www.wikidata.org/prop/direct/P3373");
        //PropertyEdge siblingInv            = new PropertyEdge("http://www.wikidata.org/prop/direct/P3373", true);
        PropertyEdge studentOf             = new PropertyEdge("http://www.wikidata.org/prop/direct/P1066");
        PropertyEdge eyeColor              = new PropertyEdge("http://www.wikidata.org/prop/direct/P1340");
        // PropertyEdge place of birth        = new PropertyEdge("http://www.wikidata.org/prop/direct/P19");
        // PropertyEdge date of birth         = new PropertyEdge("http://www.wikidata.org/prop/direct/P569");
        PropertyEdge height                = new PropertyEdge("http://www.wikidata.org/prop/direct/P2048");
        PropertyEdge occupation            = new PropertyEdge("http://www.wikidata.org/prop/direct/P106");
        //PropertyEdge occupationInv         = new PropertyEdge("http://www.wikidata.org/prop/direct/P106", true);
        PropertyEdge residence             = new PropertyEdge("http://www.wikidata.org/prop/direct/P551");
        PropertyEdge hairColor             = new PropertyEdge("http://www.wikidata.org/prop/direct/P1884");
        PropertyEdge awardReceived         = new PropertyEdge("http://www.wikidata.org/prop/direct/P166");
        PropertyEdge filmography           = new PropertyEdge("http://www.wikidata.org/prop/direct/P1283");
        //PropertyEdge filmographyInv        = new PropertyEdge("http://www.wikidata.org/prop/direct/P1283", true);
        // PropertyEdge influenced by         = new PropertyEdge("http://www.wikidata.org/prop/direct/P737");
        // PropertyEdge work location         = new PropertyEdge("http://www.wikidata.org/prop/direct/P937");
        PropertyEdge countryOfCitzenship   = new PropertyEdge("http://www.wikidata.org/prop/direct/P27");
        //PropertyEdge countryOfCitzenshipInv= new PropertyEdge("http://www.wikidata.org/prop/direct/P27", true);
        PropertyEdge locatedIn             = new PropertyEdge("http://www.wikidata.org/prop/direct/P131");
        // PropertyEdge date of death         = new PropertyEdge("http://www.wikidata.org/prop/direct/P570");
        PropertyEdge label                 = new PropertyEdge("http://www.w3.org/2000/01/rdf-schema#label");
        PropertyEdge about                 = new PropertyEdge("http://schema.org/about");
        PropertyEdge description           = new PropertyEdge("http://schema.org/description");



        // Add edges like this: (source, target, PropertyEdge)
        navigationGraph.addEdge(classHuman, datatypeString, birthName);
        navigationGraph.addEdge(classHuman, datatypeString, givenName);
        navigationGraph.addEdge(classHuman, datatypeString, familyName);
        navigationGraph.addEdge(classHuman, datatypeString, nameNativeLanguage);
        navigationGraph.addEdge(classHuman, datatypeString, height);
        navigationGraph.addEdge(classHuman, datatypeString, eyeColor);
        navigationGraph.addEdge(classHuman, datatypeString, hairColor);
        navigationGraph.addEdge(classHuman, datatypeString, label);
        navigationGraph.addEdge(classHuman, datatypeString, about);
        navigationGraph.addEdge(classHuman, datatypeString, description);
        navigationGraph.addEdge(classHuman, datatypeInteger, numberOfChildren);

        navigationGraph.addEdge(classHuman, classGender, sexOrGender);
        //navigationGraph.addEdge(classGender, classHuman, sexOrGenderInv);
        navigationGraph.addEdge(classHuman, classHuman, child);
        //navigationGraph.addEdge(classHuman, classHuman, childInv);
        navigationGraph.addEdge(classHuman, classHuman, spouse);
        //navigationGraph.addEdge(classHuman, classHuman, spouseInv);
        navigationGraph.addEdge(classHuman, classHuman, sibling);
        //navigationGraph.addEdge(classHuman, classHuman, siblingInv);
        navigationGraph.addEdge(classHuman, classCountry, countryOfCitzenship);
        //navigationGraph.addEdge(classCountry, classHuman, countryOfCitzenshipInv);
        navigationGraph.addEdge(classHuman, classProfession, occupation);
        //navigationGraph.addEdge(classProfession, classHuman, occupationInv);
        navigationGraph.addEdge(classHuman, classFilmography, filmography);
        //navigationGraph.addEdge(classFilmography, classHuman, filmographyInv);

        return navigationGraph;
	}

	static void addQueries(QueryLog ql) {
		String path = "src/main/resources/wikidata/";
		String[] files = {
			path + "2017-06-12_2017-07-09.txt",
			path + "2017-07-10_2017-08-06.txt",
			path + "2017-08-07_2017-09-03.txt",
			path + "2017-12-03_2017-12-30.txt",
			path + "2018-01-01_2018-01-28.txt",
			path + "2018-01-29_2018-02-25.txt",
			path + "2018-02-26_2018-03-25.txt"
		};
		for (String file : files) ql.addQueryLogFromFile(file);
	}
}
