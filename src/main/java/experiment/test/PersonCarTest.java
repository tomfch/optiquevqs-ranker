package experiment.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.SubClassEdge;
import eu.optiquevqs.graph.navigation.ClassNode;
import eu.optiquevqs.graph.navigation.DatatypeNode;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.navigation.NavigationGraphNode;
import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.graph.query.QueryGraphFactory;
import eu.optiquevqs.ranker.association.Apriori;
import eu.optiquevqs.ranker.data_structures.EvaluationConfig;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.Item;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.RatingsMatrix;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.data_structures.SubClasses;
import eu.optiquevqs.ranker.enums.Category;
import eu.optiquevqs.ranker.enums.RankerType;
import eu.optiquevqs.ranker.enums.SimilarityMeasure;
import eu.optiquevqs.ranker.neighborhood.Neighborhood;
import eu.optiquevqs.ranker.rankers.ItemRanker;
import eu.optiquevqs.ranker.rankers.Model;
import eu.optiquevqs.ranker.rankers.Ranker;
import eu.optiquevqs.ranker.rankers.RulesRanker;
import eu.optiquevqs.ranker.utils.RankerUtils;
import evaluation.Evaluation;

public class PersonCarTest {

	public static void main(String[] args) {
		NavigationGraph ng = createNavigationGraph();
		QueryLog ql = new QueryLog(ng);
		addQueries(ql);

		// TEST START
		/*
		RankerConfig rankerConfig = new RankerConfig();
		rankerConfig.rankerType = RankerType.ITEM;
		rankerConfig.minSupport = 0.1;
		rankerConfig.minConfidence = 0.1;
		rankerConfig.similarityMeasure = SimilarityMeasure.COSINE;
		rankerConfig.decay = 0.1;
		rankerConfig.itemPropertyOnly = true;

		SimpleQueryLog sql = RankerUtils.queryLogToSimpleQueryLog(ql);
		Apriori apriori = new Apriori(sql, rankerConfig);
		RatingsMatrix matrix = apriori.getRatingsMatrix();

		Map<Integer, Set<Item>> map = matrix.getItemMap();
		for(Entry<Integer, Set<Item>> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}

		double[][] m = matrix.getMatrix();
		for(int i = 0; i < m.length; i++) {
			System.out.println(Arrays.toString(m[i]));
		}
		*/
		// TEST END 
		// TEST START
		RankerConfig rankerConfig = new RankerConfig();
		rankerConfig.decay = 0;
		rankerConfig.itemPropertyOnly = true;
		rankerConfig.minConfidence = 0.2;
		rankerConfig.minSupport = 0.2;
		rankerConfig.rankerType = RankerType.ITEM;
		rankerConfig.similarityMeasure = SimilarityMeasure.JACCARD;

		Ranker ranker = new RulesRanker(ql, rankerConfig);
		Model model = ranker.createModel();
		//model.deleteThisMethod();


		QueryGraph query = QueryGraphFactory.read("src/main/resources/test/pcq09.rq", ng);

		List<Extension> list = model.recommendExtensions(query, "http://www.example.org/Person", Category.OBJECT);
		for(Extension e : list) System.out.println(e);

		System.out.println();

		List<Extension> list1 = model.recommendExtensions(query, "http://www.example.org/Person", Category.DATATYPE);
		for(Extension e : list1) System.out.println(e);

		// TEST END 

		// TEST START
		/*

		// Evaluation parameters
		List<RankerConfig> rankerConfigs = new ArrayList<>();
		List<SimilarityMeasure> simMeasures = new ArrayList<>();
		simMeasures.add(SimilarityMeasure.COSINE);
		simMeasures.add(SimilarityMeasure.EUCLIDIAN);
		simMeasures.add(SimilarityMeasure.RUZICKA);
		simMeasures.add(SimilarityMeasure.JACCARD_OLD);
		simMeasures.add(SimilarityMeasure.JACCARD);
		for(SimilarityMeasure simMeasure : simMeasures) {
			RankerConfig rankerConfig = new RankerConfig();
			rankerConfig.rankerType = RankerType.RULES;
			rankerConfig.similarityMeasure = simMeasure;
			rankerConfig.minSupport = .1;
			rankerConfig.minConfidence = 0.1;
			rankerConfig.decay = 0.2;
			rankerConfigs.add(rankerConfig);
		}

		EvaluationConfig evaluationConfig = new EvaluationConfig();
		evaluationConfig.rankerConfigs = rankerConfigs;
		evaluationConfig.splitRatio = 0.2;
		evaluationConfig.evalCount = 100;
		evaluationConfig.minEdgeCounts = new Integer[] {2};
		evaluationConfig.levelsOfResults = 3;
		evaluationConfig.detailed = true;

		// Run evaluation
		Evaluation evaluation = new Evaluation(ql, "results/");
		evaluation.evaluate(evaluationConfig);

		System.out.println("Done!");

		*/
		// TEST END
	}


	static void addQueries(QueryLog ql) {
		ql.addQueryFromFile("src/main/resources/test/pcq01.rq", 1.0);
		ql.addQueryFromFile("src/main/resources/test/pcq02.rq", 1.0);
		ql.addQueryFromFile("src/main/resources/test/pcq03.rq", 1.0);
		ql.addQueryFromFile("src/main/resources/test/pcq04.rq", 1.0);
		ql.addQueryFromFile("src/main/resources/test/pcq05.rq", 1.0);
		ql.addQueryFromFile("src/main/resources/test/pcq06.rq", 1.0);
		ql.addQueryFromFile("src/main/resources/test/pcq07.rq", 1.0);
		ql.addQueryFromFile("src/main/resources/test/pcq08.rq", 1.0);
	}

	static NavigationGraph createNavigationGraph() {
		NavigationGraph ng = new NavigationGraph();

		// Classes
		NavigationGraphNode cPerson = new ClassNode("http://www.example.org/Person");
		NavigationGraphNode cCar = new ClassNode("http://www.example.org/Car");

		ng.addVertex(cPerson);
		ng.addVertex(cCar);

		// Data types
		NavigationGraphNode dtString = new DatatypeNode("String");
		NavigationGraphNode dtInteger = new DatatypeNode("Integer");

		ng.addVertex(dtString);
		ng.addVertex(dtInteger);

		// Object properties
		String knows = "http://www.example.org/knows";
		String owns = "http://www.example.org/owns";
		String previouslyOwned= "http://www.example.org/previouslyOwned";

		// Data properties
		String name = "http://www.example.org/name";
		String phoneNumber = "http://www.example.org/phoneNumber";
		String milage = "http://www.example.org/milage";

		// Edges
		ng.addEdge(cPerson, cPerson, new PropertyEdge(knows));
		ng.addEdge(cPerson, cCar, new PropertyEdge(owns));
		ng.addEdge(cPerson, cCar, new PropertyEdge(previouslyOwned));
		ng.addEdge(cPerson, dtString, new PropertyEdge(name));
		ng.addEdge(cPerson, dtInteger, new PropertyEdge(phoneNumber));

		ng.addEdge(cCar, dtInteger, new PropertyEdge(milage));

		return ng;
	}
}
