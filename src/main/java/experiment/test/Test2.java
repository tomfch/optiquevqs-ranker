package experiment.test;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.graph.query.QuerylogStats;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.Extensions;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.enums.Category;
import experiment.wikidata3.WikidataExperiment3;

public class Test2 {

	public static void main(String[] args) {
		NavigationGraph ng = WikidataExperiment3.createNavigationGraph();
		System.out.println("Navigation graph created");
		QueryLog ql = new QueryLog(ng);
		System.out.println("Query log instanciated");
		WikidataExperiment3.addQueries(ql);
		System.out.println("Query log loaded from file");
		ql.classAndPropertyUsageToFile("results/");
		System.out.println("Class and property usage to file");
		ql.querylogStatsToFile("results/");
		System.out.println("Querylog stats to file");
		System.out.println("Done!");
	}
}
