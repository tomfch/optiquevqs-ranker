package experiment.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.SubClassEdge;
import eu.optiquevqs.graph.navigation.ClassNode;
import eu.optiquevqs.graph.navigation.DatatypeNode;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.navigation.NavigationGraphNode;
import eu.optiquevqs.ranker.association.Apriori;
import eu.optiquevqs.ranker.data_structures.EvaluationConfig;
import eu.optiquevqs.ranker.data_structures.Item;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.RatingsMatrix;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.enums.RankerType;
import eu.optiquevqs.ranker.enums.SimilarityMeasure;
import eu.optiquevqs.ranker.neighborhood.Neighborhood;
import eu.optiquevqs.ranker.utils.RankerUtils;
import evaluation.Evaluation;

public class TestExperiment {

	public static void main(String[] args) {
		NavigationGraph ng = createNavigationGraph();
		QueryLog ql = new QueryLog(ng);
		addQueries(ql);

		// TEST START
		RankerConfig rankerConfig = new RankerConfig();
		rankerConfig.rankerType = RankerType.ITEM;
		rankerConfig.minSupport = 0.1;
		rankerConfig.minConfidence = 0.1;
		rankerConfig.similarityMeasure = SimilarityMeasure.COSINE;
		rankerConfig.decay = 0.1;
		rankerConfig.itemPropertyOnly = true;

		SimpleQueryLog sql = RankerUtils.queryLogToSimpleQueryLog(ql);
		Apriori apriori = new Apriori(sql, rankerConfig);
		RatingsMatrix matrix = apriori.getRatingsMatrix();

		Map<Integer, Set<Item>> map = matrix.getItemMap();
		for(Entry<Integer, Set<Item>> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}

		double[][] m = matrix.getMatrix();
		for(int i = 0; i < m.length; i++) {
			System.out.println(Arrays.toString(m[i]));
		}
		// TEST END 
		// TEST START
		/*
		Ranker ranker = new RulesRanker(ql);
		Model model = ranker.createModel();
		//model.deleteThisMethod();


		Iterator<QueryGraph> it = ql.iterator();
		QueryGraph query = null;
		while(it.hasNext()) {
			query = it.next();
			break;
		}

		List<Extension> list = model.recommendExtensions(query, "http://www.example.org/Person", Category.OBJECT);
		for(Extension e : list) System.out.println(e);

		System.out.println();

		SubClasses subClasses = model.recommendSubClasses(query, "http://www.example.org/Person");
		subClasses.prettyPrint();
		*/
		// TEST END 

		// TEST START
		/*

		// Evaluation parameters
		List<RankerConfig> rankerConfigs = new ArrayList<>();
		List<SimilarityMeasure> simMeasures = new ArrayList<>();
		simMeasures.add(SimilarityMeasure.COSINE);
		simMeasures.add(SimilarityMeasure.EUCLIDIAN);
		simMeasures.add(SimilarityMeasure.RUZICKA);
		simMeasures.add(SimilarityMeasure.JACCARD_OLD);
		simMeasures.add(SimilarityMeasure.JACCARD);
		for(SimilarityMeasure simMeasure : simMeasures) {
			RankerConfig rankerConfig = new RankerConfig();
			rankerConfig.rankerType = RankerType.RULES;
			rankerConfig.similarityMeasure = simMeasure;
			rankerConfig.minSupport = .1;
			rankerConfig.minConfidence = 0.1;
			rankerConfig.decay = 0.2;
			rankerConfigs.add(rankerConfig);
		}

		EvaluationConfig evaluationConfig = new EvaluationConfig();
		evaluationConfig.rankerConfigs = rankerConfigs;
		evaluationConfig.splitRatio = 0.2;
		evaluationConfig.evalCount = 100;
		evaluationConfig.minEdgeCounts = new Integer[] {2};
		evaluationConfig.levelsOfResults = 3;
		evaluationConfig.detailed = true;

		// Run evaluation
		Evaluation evaluation = new Evaluation(ql, "results/");
		evaluation.evaluate(evaluationConfig);

		System.out.println("Done!");

		*/
		// TEST END
	}


	static void addQueries(QueryLog ql) {
		ql.addQueryFromFile("src/main/resources/test/q01.rq", 1.0);
		ql.addQueryFromFile("src/main/resources/test/q02.rq", 4.0);
		ql.addQueryFromFile("src/main/resources/test/q03.rq", 3.0);
		ql.addQueryFromFile("src/main/resources/test/q04.rq", 2.0);
		ql.addQueryFromFile("src/main/resources/test/q05.rq", 3.0);
		ql.addQueryFromFile("src/main/resources/test/q06.rq", 2.0);
		ql.addQueryFromFile("src/main/resources/test/q07.rq", 5.0);
	}

	static NavigationGraph createNavigationGraph() {
		NavigationGraph ng = new NavigationGraph();

		// Classes
		NavigationGraphNode cPerson = new ClassNode("http://www.example.org/Person");
		NavigationGraphNode cPet = new ClassNode("http://www.example.org/Pet");
		NavigationGraphNode cCar = new ClassNode("http://www.example.org/Car");
		NavigationGraphNode cManufacturer = new ClassNode("http://www.example.org/Manufacturer");

		ng.addVertex(cPerson);
		ng.addVertex(cPet);
		ng.addVertex(cCar);
		ng.addVertex(cManufacturer);

		// Data types
		NavigationGraphNode dtString = new DatatypeNode("String");
		NavigationGraphNode dtInteger = new DatatypeNode("Integer");

		ng.addVertex(dtString);
		ng.addVertex(dtInteger);

		// Object properties
		String owns = "http://www.example.org/owns";
		String madeBy = "http://www.example.org/madeBy";
		String likes = "http://www.example.org/likes";
		String knows = "http://www.example.org/knows";

		// Data properties
		String name = "http://www.example.org/name";
		String age = "http://www.example.org/age";
		String email = "http://www.example.org/email";
		String regNo = "http://www.example.org/regNo";
		String color = "http://www.example.org/color";

		// Edges
		ng.addEdge(cPerson, cPerson, new PropertyEdge(likes));
		ng.addEdge(cPerson, cPerson, new PropertyEdge(knows));
		ng.addEdge(cPerson, cPet, new PropertyEdge(owns));
		ng.addEdge(cPerson, cCar, new PropertyEdge(owns));
		ng.addEdge(cPerson, dtString, new PropertyEdge(name));
		ng.addEdge(cPerson, dtString, new PropertyEdge(email));
		ng.addEdge(cPerson, dtInteger, new PropertyEdge(age));

		ng.addEdge(cPet, dtString, new PropertyEdge(name));
		ng.addEdge(cPet, dtInteger, new PropertyEdge(age));

		ng.addEdge(cCar, cManufacturer, new PropertyEdge(madeBy));
		ng.addEdge(cCar, dtString, new PropertyEdge(regNo));
		ng.addEdge(cCar, dtString, new PropertyEdge(color));

		ng.addEdge(cManufacturer, dtString, new PropertyEdge(name));

		// Subclasses
		NavigationGraphNode cMan = new ClassNode("http://www.example.org/Man");
		NavigationGraphNode cWoman = new ClassNode("http://www.example.org/Woman");
		NavigationGraphNode cElectricCar = new ClassNode("http://www.example.org/ElectricCar");
		ng.addVertex(cMan);
		ng.addVertex(cWoman);
		ng.addVertex(cElectricCar);
        ng.addEdge(cMan, cPerson, new SubClassEdge());
        ng.addEdge(cWoman, cPerson, new SubClassEdge());
        ng.addEdge(cElectricCar, cCar, new SubClassEdge());

        String batteryCapacity = "http://www.example.org/batteryCapacity";
        ng.addEdge(cElectricCar, dtInteger, new PropertyEdge(batteryCapacity));

		return ng;
	}
}
