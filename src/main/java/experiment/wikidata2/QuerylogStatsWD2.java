package experiment.wikidata2;

import java.util.Map;
import java.util.Map.Entry;

import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.data_structures.Triple;
import eu.optiquevqs.ranker.utils.RankerUtils;

public class QuerylogStatsWD2 {
	public static void main(String[] args) {
		NavigationGraph ng = WikidataExperiment2.createNavigationGraph();
		QueryLog ql = new QueryLog(ng);
		WikidataExperiment2.addQueries(ql);
		//ql.querylogStatsToFile("");
		SimpleQueryLog sql = RankerUtils.queryLogToSimpleQueryLog(ql);
		Map<Triple, Double> op = sql.getObjectPropertyItems();
		Map<Triple, Double> dp = sql.getDataPropertyItems();

		for(Double v : dp.values()) {
			System.out.println(v);
		}
		for(Double v : op.values()) {
			System.out.println(v);
		}
	}
}
