package experiment.music;

import eu.optiquevqs.graph.PropertyEdge;
import eu.optiquevqs.graph.navigation.ClassNode;
import eu.optiquevqs.graph.navigation.DatatypeNode;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.graph.navigation.NavigationGraphNode;
import eu.optiquevqs.ranker.data_structures.QueryLog;

// Outdated. Not in use.
public class MusicExperiment {

	public static void main(String[] args) {
		/*
		RankerType rankerType = RankerType.ITEM; // "item" or "mostPopular"
		Category category = Category.OBJECT; // "object" or "datatype"
		String focusNodeType = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#Song";

		long start = System.nanoTime();

		// Create navigation graph and query log
		NavigationGraph ng = createNavigationGraph();
		QueryLog ql = new QueryLog(ng);
		addQueries(ql);

		long queryLogRead = System.nanoTime() - start;

		// Create model
		Ranker simpleRanker = null;
		Model simpleRankerModel = null;

		switch(rankerType) {
			case ITEM :
				simpleRanker = new ItemRanker(ql);
				simpleRankerModel = simpleRanker.createModel();
				break;
			case MOSTPOPULAR :
				simpleRanker = new MostPopularRanker(ql);
				simpleRankerModel = simpleRanker.createModel();

		}

		long modelCreated = System.nanoTime() - queryLogRead - start;

		// Example result
		QueryGraph qg = null;
		Iterator<QueryGraph> it = ql.iterator();
		while(it.hasNext()) {
			qg = it.next();
			qg = it.next();
			System.out.println(qg);
			Set<QueryVariable> nodes = qg.vertexSet();
			for (QueryVariable node : nodes) {
				System.out.print(node);
				System.out.println(", " + node.getType());
			}
			break;
		}
		System.out.println("");
		Map<Item, Double> map = simpleRankerModel.getAllExtensions(qg, focusNodeType, category);
		for (Entry<Item, Double> entry : map.entrySet()) {
			Item tv = entry.getKey();
			double rank = entry.getValue();
			System.out.println(tv + " " + rank);
		}

		long finishedPredictions = System.nanoTime() - modelCreated - queryLogRead - start;

		System.out.println("Finished!");
		System.out.println("Importing query log: " + (queryLogRead / 1e9));
		System.out.println("Building model: " + (modelCreated / 1e9));
		System.out.println("Making predictions: " + (finishedPredictions / 1e9));
		System.out.println("Total time: " + ((queryLogRead + modelCreated + finishedPredictions) / 1e9));	
	*/
	}

	/*
	public static void main(String[] args) {
		System.out.println("music experiment main method");
		NavigationGraph ng = createNavigationGraph();
		QueryLog ql = new QueryLog(ng);
		addQueries(ql);

		//MostPopular
		System.out.println("MOST POPULAR RANKER");
		SimpleRanker ranker = new SimpleMostPopularRanker(ql);
		SimpleRankerModel model = ranker.createModel();
		Iterator<QueryGraph> it = ql.iterator();
		QueryGraph q = it.next();
		Map<TripleVector, Double> map = model.getAllExtensions(q, "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#Album", "datatype");
		for (Entry<TripleVector, Double> entry : map.entrySet()) {
			TripleVector tv = entry.getKey();
			double rank = entry.getValue();
			System.out.println(tv + " " + rank);
		}

		//Factorization
		System.out.println("FACTORIZATION RANKER");
		SimpleQueryLog sql = SimpleRankerUtils.queryLogToSimpleQueryLog(ql);
		//sql.writeToFiles("test/", false);
	}
	*/

	static void addQueries(QueryLog ql) {
		ql.addQueryFromFile("src/main/resources/music/q01.rq");
		ql.addQueryFromFile("src/main/resources/music/q02.rq");
		ql.addQueryFromFile("src/main/resources/music/q03.rq");
		ql.addQueryFromFile("src/main/resources/music/q04.rq");
		ql.addQueryFromFile("src/main/resources/music/q05.rq");
		ql.addQueryFromFile("src/main/resources/music/q06.rq");
		ql.addQueryFromFile("src/main/resources/music/q07.rq");
	}

	static NavigationGraph createNavigationGraph() {
		NavigationGraph ng = new NavigationGraph();

		// Classes
		NavigationGraphNode cArtist = new ClassNode("http://www.semanticweb.org/tomfc/ontologies/2019/7/music#Artist");
		NavigationGraphNode cBand = new ClassNode("http://www.semanticweb.org/tomfc/ontologies/2019/7/music#Band");
		NavigationGraphNode cPerson = new ClassNode("http://www.semanticweb.org/tomfc/ontologies/2019/7/music#Person");
		NavigationGraphNode cAlbum = new ClassNode("http://www.semanticweb.org/tomfc/ontologies/2019/7/music#Album");
		NavigationGraphNode cSong = new ClassNode("http://www.semanticweb.org/tomfc/ontologies/2019/7/music#Song");

		ng.addVertex(cArtist);
		ng.addVertex(cBand);
		ng.addVertex(cPerson);
		ng.addVertex(cAlbum);
		ng.addVertex(cSong);

		// Data types
		NavigationGraphNode dtString = new DatatypeNode("String");
		NavigationGraphNode dtInteger = new DatatypeNode("Integer"); // should be int?

		ng.addVertex(dtString);
		ng.addVertex(dtInteger);

		// Properties
		String isPerson = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#isPerson";
		String isArtist = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#isArtist";
		String inBand = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#isBand";
		String hasMember = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#hasMember";
		String hasMade = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#hasMade";
		String madeBy = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#madeBy";
		String onAlbum = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#onAlbum";
		String hasSong = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#hasSong";
		String hasWritten = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#hasWritten";
		String writtenBy = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#writtenBy";

		String label = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#label";
		String bio = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#bio";
		String foundedIn = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#foundedIn";
		String name = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#name";
		String born = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#born";
		String died = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#died";
		String released = "http://www.semanticweb.org/tomfc/ontologies/2019/7/music#released";

		// Connections
		ng.addEdge(cArtist, cPerson, new PropertyEdge(isPerson));
		ng.addEdge(cArtist, cAlbum, new PropertyEdge(hasMade));
		ng.addEdge(cArtist, dtString, new PropertyEdge(label));
		ng.addEdge(cArtist, dtString, new PropertyEdge(bio));

		ng.addEdge(cBand, cPerson, new PropertyEdge(isPerson));
		ng.addEdge(cBand, cAlbum, new PropertyEdge(hasMade));
		ng.addEdge(cBand, cPerson, new PropertyEdge(hasMember));
		ng.addEdge(cBand, dtString, new PropertyEdge(label));
		ng.addEdge(cBand, dtString, new PropertyEdge(bio));
		ng.addEdge(cBand, dtInteger, new PropertyEdge(foundedIn));

		ng.addEdge(cPerson, cArtist, new PropertyEdge(isArtist));
		ng.addEdge(cPerson, cBand, new PropertyEdge(inBand));
		ng.addEdge(cPerson, cSong, new PropertyEdge(hasWritten));
		ng.addEdge(cPerson, dtString, new PropertyEdge(name));
		ng.addEdge(cPerson, dtInteger, new PropertyEdge(born));
		ng.addEdge(cPerson, dtInteger, new PropertyEdge(died));

		ng.addEdge(cAlbum, cArtist, new PropertyEdge(madeBy));
		ng.addEdge(cAlbum, cSong, new PropertyEdge(hasSong));
		ng.addEdge(cAlbum, dtString, new PropertyEdge(label));
		ng.addEdge(cAlbum, dtInteger, new PropertyEdge(released));

		ng.addEdge(cSong, cAlbum, new PropertyEdge(onAlbum));
		ng.addEdge(cSong, cPerson, new PropertyEdge(writtenBy));
		ng.addEdge(cSong, dtString, new PropertyEdge(label));
		ng.addEdge(cSong, dtInteger, new PropertyEdge(released));

		return ng;
	}
}
