package experiment.music;

import java.util.Iterator;

import org.apache.jena.graph.Triple;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.sparql.algebra.Algebra;
import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.op.OpBGP;
import org.apache.jena.sparql.algebra.op.OpFilter;
import org.apache.jena.sparql.algebra.op.OpPath;
import org.apache.jena.sparql.algebra.op.OpSequence;
import org.apache.jena.sparql.algebra.optimize.TransformPathFlattern;
import org.apache.jena.sparql.core.BasicPattern;

public class TestInverseEdges {
	public static void main(String[] args) {
		System.out.println("--- 1 ---");
		String f1= "src/main/resources/music/q07.rq";
		Query q1 = QueryFactory.read(f1);
		BasicPattern p1 = queryToBasicPattern(q1);
		System.out.println(p1);

		System.out.println();
		System.out.println("--- 2 ---");
		String f2= "src/main/resources/music/q8.rq";
		Query q2 = QueryFactory.read(f2);
		BasicPattern p2 = queryToBasicPattern(q2);
		System.out.println(p2);
	}

	private static BasicPattern queryToBasicPattern(Query query) {
		Op op = Algebra.compile(query);
		if (op instanceof OpFilter) op = ((OpFilter)op).getSubOp();
		if (op instanceof OpSequence) return opSequenceToBasicPattern((OpSequence)op);
		OpBGP opBGP = (OpBGP)op;
		return opBGP.getPattern();
	}

	private static BasicPattern opSequenceToBasicPattern(OpSequence op) {
		BasicPattern pattern = new BasicPattern();
		Iterator<Op> it = ((OpSequence)op).iterator();
		while(it.hasNext()) {
			Op current = it.next();
			if(current instanceof OpPath) {
				current = ((OpPath)current).apply(new TransformPathFlattern());
			}
			BasicPattern subPattern = ((OpBGP)current).getPattern();
			Triple subTriple = subPattern.get(0);
			pattern.add(subTriple);
		}
		return pattern;
	}

}
