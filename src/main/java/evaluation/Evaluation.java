package evaluation;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.graph.query.QueryVariable;
import eu.optiquevqs.ranker.data_structures.EvaluationConfig;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.ResultPerRecommend;
import eu.optiquevqs.ranker.data_structures.ResultPerRun;
import eu.optiquevqs.ranker.data_structures.Results;
import eu.optiquevqs.ranker.data_structures.SubClasses;
import eu.optiquevqs.ranker.data_structures.TestQuery;
import eu.optiquevqs.ranker.data_structures.Triple;
import eu.optiquevqs.ranker.enums.Category;
import eu.optiquevqs.ranker.enums.RecommendationType;
import eu.optiquevqs.ranker.rankers.Model;
import eu.optiquevqs.ranker.utils.EvaluationUtils;

public class Evaluation {
	private QueryLog querylog;
	private String resultsPath;
	private EvaluationConfig evalConfig;
	private EvaluationToFile evalToFile;
	private RankerConfig rankerConfig;
	private int minEdgeCount;

	public Evaluation(QueryLog querylog, String resultsPath) {
		this.querylog = querylog;
		this.resultsPath = resultsPath;
		this.evalConfig = null;
		this.evalToFile = null;
	}

	public void evaluate(EvaluationConfig evaluationConfig) {
		this.evalConfig = evaluationConfig;
		this.evalToFile = new EvaluationToFile(this.resultsPath, evaluationConfig);
		for(RankerConfig rankerConfig : this.evalConfig.rankerConfigs) {
			this.rankerConfig = rankerConfig;
			for(Integer minEdgeCount : evalConfig.minEdgeCounts) {
				this.minEdgeCount = minEdgeCount;
				this.calculateResults();
			}
			if(evaluationConfig.printProgress) {
				System.out.println(rankerConfig.rankerType + " complete.");
			}
		}
		evalToFile.close();
	}

	private void calculateResults() {
		Results extensionResults = new Results(RecommendationType.EXTENSION);
		Results subclassResults = null;
		if(!this.evalConfig.onlyExtensionRecommends) {
			subclassResults = new Results(RecommendationType.SUBCLASS);
		} 
		for(int runNo = 0; runNo < this.evalConfig.evalCount; runNo++) {
			ResultPerRun[] resultsPerRun = this.calculateResultsPerRun();
			extensionResults.add(resultsPerRun[0]);
			if(!this.evalConfig.onlyExtensionRecommends) {
				subclassResults.add(resultsPerRun[1]);
			}
		}
		evalToFile.appendResultsFile(this.rankerConfig, this.minEdgeCount, extensionResults);
		if(!this.evalConfig.onlyExtensionRecommends) {
			evalToFile.appendResultsFile(this.rankerConfig, this.minEdgeCount, subclassResults);
		}
	}

	private ResultPerRun[] calculateResultsPerRun() {
		double splitDeviation;
		QueryLog trainingSet;
		QueryLog testSet;

		int i = 0;
		do {
			EvaluationUtils.infiniteLoopCheck(i++);
			List<QueryLog> split = EvaluationUtils.singleSplit(this.querylog, evalConfig.splitRatio);
			trainingSet = split.get(0);
			testSet = split.get(1);
			splitDeviation = this.calculateSplitDeviation(testSet);
		} while(splitDeviation > 0.01);

		Model model = EvaluationUtils.trainModel(trainingSet, this.rankerConfig);
		ResultPerRun extensionResultPerRun = this.extensionRecommends(model, testSet);
		ResultPerRun subclassResultPerRun = null;
		if(!this.evalConfig.onlyExtensionRecommends) {
			subclassResultPerRun = this.subclassRecommends(model, testSet);
		}
		if(this.evalConfig.levelsOfResults >= 2) {
			evalToFile.appendPerRunFile(this.rankerConfig,
										this.minEdgeCount,
										extensionResultPerRun,
										subclassResultPerRun);
		}
		return new ResultPerRun[] {extensionResultPerRun, subclassResultPerRun};
	}

	private ResultPerRun extensionRecommends(Model model, QueryLog testSet) {
		ResultPerRun extensionResultPerRun = new ResultPerRun(RecommendationType.EXTENSION);

		Iterator<QueryGraph> it = testSet.iterator();
		while(it.hasNext()) {
			QueryGraph currentQuery = it.next();
			if(currentQuery.edgeSet().size() < this.minEdgeCount) continue;
			TestQuery testQuery = EvaluationUtils.createTestQuery(currentQuery);
			ResultPerRecommend result = this.extensionRecommend(model, testQuery, currentQuery);
			extensionResultPerRun.add(result);
		}
		return extensionResultPerRun;
	}

	private ResultPerRecommend extensionRecommend(Model model, TestQuery testQuery, QueryGraph originalQuery) {
		QueryGraph query = testQuery.getQuery();
		String focusType = testQuery.getFocusNodeType();
		Category category = testQuery.getCategory();
		Triple correctTriple = testQuery.getCorrectTriple();

		List<Extension> extensions = model.recommendExtensions(query, focusType, category);

		int i = 1;
		for(Extension extension : extensions) {
			Triple recommendedTriple = extension.getTriple();
			if(recommendedTriple.equals(correctTriple)) {
				ResultPerRecommend res = new ResultPerRecommend();
				res.recommendationType = RecommendationType.EXTENSION;
				res.weight = testQuery.getWeight();
				res.rank = i;
				res.RHR = 1.0 / i;
				if(this.evalConfig.levelsOfResults == 3) {
					evalToFile.appendExtPerRecFile(this.rankerConfig,
												   this.minEdgeCount,
												   res,
												   testQuery,
												   extensions,
												   originalQuery);
				}
				return res;
			}
			i++;
		}
		throw new NoSuchElementException("Correct extensions not present in suggested extensions.");
	}

	private ResultPerRun subclassRecommends(Model model, QueryLog testSet) {
		ResultPerRun subclassResultPerRun = new ResultPerRun(RecommendationType.SUBCLASS);

		Iterator<QueryGraph> it = testSet.iterator();
		while(it.hasNext()) {
			QueryGraph currentQuery = it.next();
			if(currentQuery.edgeSet().size() < this.minEdgeCount) continue;
			ResultPerRecommend result = this.subclassRecommend(model, currentQuery);
			subclassResultPerRun.add(result);
		}
		return subclassResultPerRun;
	}

	private ResultPerRecommend subclassRecommend(Model model, QueryGraph query) {
		Set<QueryVariable> nodes = query.classNodeSet();
		QueryVariable randomNode = EvaluationUtils.getRandomElement(nodes);
		String randomNodeType = randomNode.getType();
		SubClasses subclassRecommends = model.recommendSubClasses(query, randomNodeType);
		int rank = subclassRecommends.rankOf(randomNodeType);
		ResultPerRecommend res = new ResultPerRecommend();
		res.recommendationType = RecommendationType.SUBCLASS;
		res.weight = query.getWeight();
		res.rank = rank;
		res.RHR = 1.0 / rank;
		if(this.evalConfig.levelsOfResults == 3) {
			evalToFile.appendSubPerRecFile(this.rankerConfig,
										   this.minEdgeCount,
										   res,
										   query,
										   randomNodeType,
										   subclassRecommends);
		}
		return res;
	}

	private double calculateSplitDeviation(QueryLog testSet) {
		double targetSplitRatio = this.evalConfig.splitRatio;
		double actualSplitRatio = testSet.getTotWeight() / this.querylog.getTotWeight();
		return Math.abs((actualSplitRatio - targetSplitRatio) / targetSplitRatio);
	}
}
