package evaluation;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.graph.query.QueryVariable;
import eu.optiquevqs.ranker.data_structures.EvaluationConfig;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.QueryLog;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.ResultAggregation;
import eu.optiquevqs.ranker.data_structures.ResultPerRecommend;
import eu.optiquevqs.ranker.data_structures.SimpleQueryLog;
import eu.optiquevqs.ranker.data_structures.SubClasses;
import eu.optiquevqs.ranker.data_structures.TestQuery;
import eu.optiquevqs.ranker.data_structures.Triple;
import eu.optiquevqs.ranker.enums.Category;
import eu.optiquevqs.ranker.enums.RecommendationType;
import eu.optiquevqs.ranker.rankers.Model;
import eu.optiquevqs.ranker.utils.EvaluationUtils;
import eu.optiquevqs.ranker.utils.RankerUtils;

// Aggregates rank across several runs
public class Evaluation2 {
	private QueryLog querylog;
	private SimpleQueryLog simpleQuerylog;
	private String resultsPath;
	private EvaluationConfig evalConfig;
	private EvaluationToFile2 evalToFile;
	private RankerConfig rankerConfig;
	private int minEdgeCount;
	private int exactEdgeCount;
	private ResultAggregation resultAgg;

	public Evaluation2(QueryLog querylog, String resultsPath) {
		this.querylog = querylog;
		this.simpleQuerylog = RankerUtils.queryLogToSimpleQueryLog(querylog);
		this.resultsPath = resultsPath;
		this.evalConfig = null;
		this.evalToFile = null;
	}
	
	public void evaluate(EvaluationConfig evalConfig) {
		this.evalConfig = evalConfig;
		this.evalToFile = new EvaluationToFile2(this.resultsPath, evalConfig);
		for(RankerConfig rankerConfig : this.evalConfig.rankerConfigs) {
			this.rankerConfig = rankerConfig;
			for(Integer minEdgeCount : evalConfig.minEdgeCounts) {
				this.minEdgeCount = minEdgeCount;
				for(Integer exactEdgeCount : evalConfig.exactEdgeCounts) {
					this.exactEdgeCount = exactEdgeCount;
					this.calculateResults();
				}
			}
			if(evalConfig.printProgress) System.out.println(rankerConfig.rankerType + " complete.");
		}
		this.evalToFile.close();
	}

	private void calculateResults() {
		this.resultAgg = new ResultAggregation(this.evalConfig.recType);
		for(int runNo = 0; runNo < this.evalConfig.evalCount; runNo++) {
			this.singleRun();
		}
		this.evalToFile.appendResultsFile(this.rankerConfig, this.minEdgeCount, this.exactEdgeCount, this.resultAgg);
		if(this.evalConfig.levelsOfResults == 3) {
			this.evalToFile.appendPerRankFile(this.rankerConfig, this.minEdgeCount, this.exactEdgeCount, this.resultAgg);
		}
	}

	private void singleRun() {
		double splitDeviation;
		QueryLog trainingSet;
		QueryLog testSet;

		int i = 0;
		do {
			EvaluationUtils.infiniteLoopCheck(i++);
			List<QueryLog> split = EvaluationUtils.singleSplit(this.querylog, evalConfig.splitRatio);
			trainingSet = split.get(0);
			testSet = split.get(1);
			splitDeviation = this.calculateSplitDeviation(testSet);
		} while(splitDeviation > 0.01);

		Model model = EvaluationUtils.trainModel(trainingSet, this.rankerConfig);
		if(this.evalConfig.recType == RecommendationType.EXTENSION) this.extensionRecommends(model, testSet);
		if(this.evalConfig.recType == RecommendationType.SUBCLASS) this.subclassRecommends(model, testSet);
	}

	private void extensionRecommends(Model model, QueryLog testSet) {
		Iterator<QueryGraph> it = testSet.iterator();
		while(it.hasNext()) {
			QueryGraph currentQuery = it.next();
			if(this.exactEdgeCount != -1 && currentQuery.edgeSet().size() != this.exactEdgeCount) continue;
			if(currentQuery.edgeSet().size() < this.minEdgeCount) continue;
			boolean queryIsSameEdgeQuery = EvaluationUtils.queryIsSameEdgeQuery(currentQuery);
			if(this.evalConfig.excludeSameEdgeQueries && queryIsSameEdgeQuery) continue;
			if(this.evalConfig.onlySameEdgeQueries && !queryIsSameEdgeQuery) continue;
			TestQuery testQuery = EvaluationUtils.createTestQuery(currentQuery);
			boolean isLongTailProp = this.simpleQuerylog.isLongTailProperty(testQuery.getCorrectTriple());
			if(this.evalConfig.excludeLongTailProperties && isLongTailProp) continue;
			if(this.evalConfig.onlyLongTailProperties && !isLongTailProp) continue;
			this.extensionRecommend(model, testQuery, currentQuery);
		}
	}

	private void extensionRecommend(Model model, TestQuery testQuery, QueryGraph originalQuery) {
		QueryGraph query = testQuery.getQuery();
		String focusType = testQuery.getFocusNodeType();
		Category category = testQuery.getCategory();
		Triple correctTriple = testQuery.getCorrectTriple();

		List<Extension> extensions = model.recommendExtensions(query, focusType, category);
		
		int i = 1;
		for(Extension extension : extensions) {
			Triple recommendedTriple = extension.getTriple();
			if(recommendedTriple.equals(correctTriple)) {
				ResultPerRecommend res = new ResultPerRecommend();
				res.recommendationType = RecommendationType.EXTENSION;
				res.weight = testQuery.getWeight();
				res.rank = i;
				res.RHR = 1.0 / i;
				this.resultAgg.add(res);
				if(this.evalConfig.levelsOfResults == 3) {
					this.evalToFile.appendExtPerRecFile(this.rankerConfig,
														this.minEdgeCount,
														this.exactEdgeCount,
														res,
														testQuery,
														extensions,
														originalQuery);
				}
				return;
			}
			i++;
		}
		throw new NoSuchElementException("Correct extensions not present in suggested extensions.");
	}

	private void subclassRecommends(Model model, QueryLog testSet) {
		Iterator<QueryGraph> it = testSet.iterator();
		while(it.hasNext()) {
			QueryGraph currentQuery = it.next();
			if(this.exactEdgeCount != -1 && currentQuery.edgeSet().size() != this.exactEdgeCount) continue;
			if(currentQuery.edgeSet().size() < this.minEdgeCount) continue;
			boolean queryIsSameEdgeQuery = EvaluationUtils.queryIsSameEdgeQuery(currentQuery);
			if(this.evalConfig.excludeSameEdgeQueries && queryIsSameEdgeQuery) continue;
			if(this.evalConfig.onlySameEdgeQueries && !queryIsSameEdgeQuery) continue;
			this.subclassRecommend(model, currentQuery);
		}
	}

	private void subclassRecommend(Model model, QueryGraph query) {
		Set<QueryVariable> nodes = query.classNodeSet();
		QueryVariable randomNode = EvaluationUtils.getRandomElement(nodes);
		String randomNodeType = randomNode.getType();
		SubClasses subclassRecommends = model.recommendSubClasses(query, randomNodeType);
		int rank = subclassRecommends.rankOf(randomNodeType);
		ResultPerRecommend res = new ResultPerRecommend();
		res.recommendationType = RecommendationType.SUBCLASS;
		res.weight = query.getWeight();
		res.rank = rank;
		res.RHR = 1.0 / rank;
		this.resultAgg.add(res);
		if(this.evalConfig.levelsOfResults == 3) {
			this.evalToFile.appendSubPerRecFile(rankerConfig,
												this.minEdgeCount,
												this.exactEdgeCount,
												res,
												query,
												randomNodeType,
												subclassRecommends);
		}
	}

	private double calculateSplitDeviation(QueryLog testSet) {
		double targetSplitRatio = this.evalConfig.splitRatio;
		double actualSplitRatio = testSet.getTotWeight() / this.querylog.getTotWeight();
		return Math.abs((actualSplitRatio - targetSplitRatio) / targetSplitRatio);
	}
}
