package evaluation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.ranker.data_structures.EvaluationConfig;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.ResultAggregation;
import eu.optiquevqs.ranker.data_structures.ResultPerRecommend;
import eu.optiquevqs.ranker.data_structures.SubClasses;
import eu.optiquevqs.ranker.data_structures.TestQuery;
import eu.optiquevqs.ranker.enums.RecommendationType;
import eu.optiquevqs.ranker.utils.EvaluationUtils;

public class EvaluationToFile2 {
	private String path;
	private EvaluationConfig evalConfig;
	private File results;
	private File resultsPerRecommend;
	private File resultsPerRank;
	private PrintWriter resultWriter;
	private PrintWriter perRecommendWriter;
	private PrintWriter perRankWriter;

	public EvaluationToFile2(String path, EvaluationConfig evalConfig) {
		this.path = path;
		this.evalConfig = evalConfig;
		this.createFiles();
		this.createFileWriters();
		this.createHeaders();
	}

	public void close() {
		this.resultWriter.close();
		if(this.evalConfig.levelsOfResults < 3) return;
		this.perRecommendWriter.close();
		this.perRankWriter.close();
	}

	private void createFiles() {
		this.results = new File(this.path + "results.csv");
		if(this.evalConfig.levelsOfResults < 3) return;
		this.resultsPerRecommend = new File(this.path + "resultsPerRecommend.csv");
		this.resultsPerRank = new File(this.path + "resultsPerRank.csv");
	}

	private void createFileWriters() {
		try {
			FileWriter fw = new FileWriter(this.results, false);
			BufferedWriter bw = new BufferedWriter(fw);
			this.resultWriter = new PrintWriter(bw);
		} catch (IOException e) {
			e.printStackTrace();
		} 

		if(this.evalConfig.levelsOfResults < 3) return;

		try {
			FileWriter fw = new FileWriter(this.resultsPerRecommend, false);
			BufferedWriter bw = new BufferedWriter(fw);
			this.perRecommendWriter = new PrintWriter(bw);
		} catch (IOException e) {
			e.printStackTrace();
		} 

		try {
			FileWriter fw = new FileWriter(this.resultsPerRank, false);
			BufferedWriter bw = new BufferedWriter(fw);
			this.perRankWriter = new PrintWriter(bw);
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

	private void createHeaders() {
		String commonHeader = 	"RankerType|" +
								"RecommendationType|" +
								"SimilarityMeasure|" +
								"MinSupport|" +
								"MinConfidence|" +
								"Decay|" +
								"MinEdgeCount|" +
								"exactEdgeCount|";
		String perRecHeader =	"weight|" +
								"RHR|" +
								"rank|";
		String resultsHeader =	"ARHR|" +
								"RHRstdev|" +
								"avgRank|" +
								"rankStdev|";
		String perRankHeader =  "rank|" +
								"weight|";
		if(this.evalConfig.detailed) {
			perRecHeader +=	"testQuery|" +
							"focusNodeType|" +
							"correctSuggestion|" +
							"top3Suggestions|" +
							"orgQueryEdgeCount|" +
							"orgQueryUniqueEdges|" +
							"orgQueryDiameter|" +
							"orgQueryMaxOutDegree|";
		}

		resultWriter.println(commonHeader + resultsHeader);
		if(this.evalConfig.levelsOfResults < 3) return;
		perRecommendWriter.println(commonHeader + perRecHeader);
		perRankWriter.println(commonHeader + perRankHeader);
	}
	
	public void appendResultsFile(RankerConfig rankerConfig,
								  int minEdgeCount,
								  int exactEdgeCount,
								  ResultAggregation resultAgg) {
		String info = this.info(rankerConfig, resultAgg.getRecommendationType(), minEdgeCount, exactEdgeCount);

		String results = resultAgg.getARHR()
				+ "|" +
				resultAgg.getRHRstdev()
				+ "|" +
				resultAgg.getAvgRank()
				+ "|" +
				resultAgg.getRankStdev()
				+ "|";

		resultWriter.println(info + results);
	}

	public void appendExtPerRecFile(RankerConfig rankerConfig,
		 							int minEdgeCount,
		 							int exactEdgeCount,
		 							ResultPerRecommend res,
		 							TestQuery testQuery,
		 							List<Extension> extensions,
		 							QueryGraph originalQuery) {
		String info = this.info(rankerConfig, res.recommendationType, minEdgeCount, exactEdgeCount);

		String results = res.weight
				+ "|" +
				res.RHR
				+ "|" +
				res.rank
				+ "|";

		if(this.evalConfig.detailed) {
			String details = testQuery.getQuery().toPrettyString()
					+ "|" +
					testQuery.getFocusNodeType()
					+ "|" +
					testQuery.getCorrectProperty()
					+ "|" +
					EvaluationUtils.top3(extensions)
					+ "|" +
					originalQuery.edgeCount()
					+ "|" +
					originalQuery.uniqueEdgeCount()
					+ "|" +
					originalQuery.diameter()
					+ "|" +
					originalQuery.maxOutDegree()
					;
			perRecommendWriter.println(info + results + details);
		} else {
			perRecommendWriter.println(info + results);
		}
	}

	public void appendPerRankFile(RankerConfig rankerConfig,
								  int minEdgeCount,
								  int exactEdgeCount,
								  ResultAggregation resultAgg) {
		String info = this.info(rankerConfig, resultAgg.getRecommendationType(), minEdgeCount, exactEdgeCount);

		Map<Integer, Double> rankWeightDistribution = resultAgg.getRankWeightDistribution();
		int maxRank = Collections.max(rankWeightDistribution.keySet());
		for(int rank = 1; rank <= maxRank; rank++) {
			double weight = rankWeightDistribution.getOrDefault(rank, 0.0);
			String results = rank + "|" + weight + "|";
			this.perRankWriter.println(info + results);
		}
	}

	public void appendSubPerRecFile(RankerConfig rankerConfig,
		 							int minEdgeCount,
		 							int exactEdgeCount,
		 							ResultPerRecommend res,
		 							QueryGraph query,
		 							String focusNodeType,
		 							SubClasses subClasses) {

		String info = this.info(rankerConfig, res.recommendationType, minEdgeCount, exactEdgeCount);

		String results = res.weight
				+ "|" +
				res.RHR
				+ "|" +
				res.rank
				+ "|";

		if(this.evalConfig.detailed) {
			String details = query
					+ "|" +
					focusNodeType
					+ "|" +
					focusNodeType
					+ "|" +
					subClasses.top3()
					+ "|" +
					query.edgeCount()
					+ "|" +
					query.uniqueEdgeCount()
					+ "|" +
					query.diameter()
					+ "|" +
					query.maxOutDegree()
					;
			perRecommendWriter.println(info + results + details);
		} else {
			perRecommendWriter.println(info + results);
		}
	}



	private String info(RankerConfig rankerConfig,
						RecommendationType recommendationType,
						int minEdgeCount,
						int exactEdgeCount) {
		return rankerConfig.rankerType
				+ "|" +
				recommendationType
				+ "|" +
				rankerConfig.similarityMeasure
				+ "|" +
				rankerConfig.minSupport
				+ "|" +
				rankerConfig.minConfidence
				+ "|" +
				rankerConfig.decay
				+ "|" +
				minEdgeCount
				+ "|" +
				exactEdgeCount
				+ "|";
	}
}

