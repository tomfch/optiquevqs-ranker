package evaluation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.ranker.data_structures.EvaluationConfig;
import eu.optiquevqs.ranker.data_structures.Extension;
import eu.optiquevqs.ranker.data_structures.RankerConfig;
import eu.optiquevqs.ranker.data_structures.ResultPerRecommend;
import eu.optiquevqs.ranker.data_structures.ResultPerRun;
import eu.optiquevqs.ranker.data_structures.Results;
import eu.optiquevqs.ranker.data_structures.SubClasses;
import eu.optiquevqs.ranker.data_structures.TestQuery;
import eu.optiquevqs.ranker.enums.RecommendationType;
import eu.optiquevqs.ranker.utils.EvaluationUtils;

public class EvaluationToFile {
	private String path;
	private EvaluationConfig evalConfig;
	private File results;
	private File resultsPerRun;
	private File resultsPerRecommend;
	private PrintWriter resultWriter;
	private PrintWriter perRunWriter;
	private PrintWriter perRecommendWriter;

	public EvaluationToFile(String path, EvaluationConfig evalConfig) {
		this.path = path;
		this.evalConfig = evalConfig;
		this.createFiles();
		this.createFileWriters();
		this.createHeaders();
	}

	public void close() {
		this.resultWriter.close();
		if(this.evalConfig.levelsOfResults < 2) return;
		this.perRunWriter.close();
		if(this.evalConfig.levelsOfResults < 3) return;
		this.perRecommendWriter.close();
	}

	private void createFiles() {
		
		this.results = new File(this.path + "results.csv");
		if(this.evalConfig.levelsOfResults < 2) return;
		this.resultsPerRun = new File(this.path + "resultsPerRun.csv");
		if(this.evalConfig.levelsOfResults < 3) return;
		this.resultsPerRecommend = new File(this.path + "resultsPerRecommend.csv");
	}

	private void createFileWriters() {
		try {
			FileWriter fw = new FileWriter(this.results, false);
			BufferedWriter bw = new BufferedWriter(fw);
			this.resultWriter = new PrintWriter(bw);
		} catch (IOException e) {
			e.printStackTrace();
		} 

		if(this.evalConfig.levelsOfResults < 2) return;

		try {
			FileWriter fw = new FileWriter(this.resultsPerRun, false);
			BufferedWriter bw = new BufferedWriter(fw);
			this.perRunWriter = new PrintWriter(bw);
		} catch (IOException e) {
			e.printStackTrace();
		} 

		if(this.evalConfig.levelsOfResults < 3) return;

		try {
			FileWriter fw = new FileWriter(this.resultsPerRecommend, false);
			BufferedWriter bw = new BufferedWriter(fw);
			this.perRecommendWriter = new PrintWriter(bw);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
	}

	private void createHeaders() {
		String commonHeader = 	"RankerType|" +
								"RecommendationType|" +
								"SimilarityMeasure|" +
								"MinSupport|" +
								"MinConfidence|" +
								"Decay|" +
								"MinEdgeCount|";
		String perRecHeader =	"weight|" +
								"RHR|" +
								"rank|";
		String perRunHeader =	"ARHR|" +
								"RHRstdev|" +
								"avgRank|" +
								"rankStdev|";
		String resultsHeader =	"avgARHR|" +
								"ARHRstdev|" +
								"avgAvgRank|" +
								"avgRankStdev|";
		if(this.evalConfig.detailed) {
			perRecHeader +=	"testQuery|" +
							"focusNodeType|" +
							"correctSuggestion|" +
							"top3Suggestions|" +
							"orgQueryEdgeCount|" +
							"orgQueryUniqueEdges|" +
							"orgQueryDiameter|" +
							"orgQueryMaxOutDegree|";
			perRunHeader +=	"totWeight|" +
							"uniqueQueryCount|";
		}

		resultWriter.println(commonHeader + resultsHeader);
		if(this.evalConfig.levelsOfResults < 2) return;
		perRunWriter.println(commonHeader + perRunHeader);
		if(this.evalConfig.levelsOfResults < 3) return;
		perRecommendWriter.println(commonHeader + perRecHeader);
	}

	public void appendResultsFile(RankerConfig rankerConfig,
								  int minEdgeCount,
								  Results allResults) {
		String info = this.info(rankerConfig, allResults.getRecommendationType(), minEdgeCount);

		String results = allResults.getAvgARHR()
						 + "|" +
						 allResults.getARHRstdev()
						 + "|" +
						 allResults.getAvgAvgRank()
						 + "|" +
						 allResults.getAvgRankStdev()
						 + "|";

		resultWriter.println(info + results);
	}

	public void appendPerRunFile(RankerConfig rankerConfig,
								 int minEdgeCount,
								 ResultPerRun extensionResults,
								 ResultPerRun subclassResults) {

		ResultPerRun[] resultsPerRun = new ResultPerRun[] {extensionResults, subclassResults};
		if(this.evalConfig.onlyExtensionRecommends) {
			resultsPerRun = new ResultPerRun[] {extensionResults};
		}
		for(ResultPerRun resultPerRun : resultsPerRun) {
			String info = this.info(rankerConfig, resultPerRun.getRecommendationType(), minEdgeCount);

			String results = resultPerRun.getARHR()
							 + "|" +
							 resultPerRun.getRHRstdev()
							 + "|" +
							 resultPerRun.getAvgRank()
							 + "|" +
							 resultPerRun.getRankStdev()
							 + "|";

			if(this.evalConfig.detailed) {
				String details = resultPerRun.getWeightAccumulated()
								 + "|" +
								 resultPerRun.getUniqueQueryCount()
								 + "|";
				perRunWriter.println(info + results + details);
			} else {
				perRunWriter.println(info + results);
			}
		}
	}

	public void appendExtPerRecFile(RankerConfig rankerConfig,
								 	int minEdgeCount,
								 	ResultPerRecommend res,
								 	TestQuery testQuery,
								 	List<Extension> extensions,
								 	QueryGraph originalQuery) {
		String info = this.info(rankerConfig, res.recommendationType, minEdgeCount);

		String results = res.weight
						 + "|" +
						 res.RHR
						 + "|" +
						 res.rank
						 + "|";

		if(this.evalConfig.detailed) {
			String details = testQuery.getQuery().toPrettyString()
							 + "|" +
							 testQuery.getFocusNodeType()
							 + "|" +
							 testQuery.getCorrectProperty()
							 + "|" +
							 EvaluationUtils.top3(extensions)
							 + "|" +
							 originalQuery.edgeCount()
							 + "|" +
							 originalQuery.uniqueEdgeCount()
							 + "|" +
							 originalQuery.diameter()
							 + "|" +
							 originalQuery.maxOutDegree()
							 ;
			perRecommendWriter.println(info + results + details);
		} else {
			perRecommendWriter.println(info + results);
		}
	}

	public void appendSubPerRecFile(RankerConfig rankerConfig,
								 	int minEdgeCount,
								 	ResultPerRecommend res,
								 	QueryGraph query,
								 	String focusNodeType,
								 	SubClasses subClasses) {

		String info = this.info(rankerConfig, res.recommendationType, minEdgeCount);

		String results = res.weight
						 + "|" +
						 res.RHR
						 + "|" +
						 res.rank
						 + "|";

		if(this.evalConfig.detailed) {
			String details = query
							 + "|" +
							 focusNodeType
							 + "|" +
							 focusNodeType
							 + "|" +
							 subClasses.top3()
							 + "|" +
							 query.edgeCount()
							 + "|" +
							 query.uniqueEdgeCount()
							 + "|" +
							 query.diameter()
							 + "|" +
							 query.maxOutDegree()
							 ;
			perRecommendWriter.println(info + results + details);
		} else {
			perRecommendWriter.println(info + results);
		}
	}

	private String info(RankerConfig rankerConfig,
						RecommendationType recommendationType,
						int minEdgeCount) {
		return rankerConfig.rankerType
			   + "|" +
			   recommendationType
			   + "|" +
			   rankerConfig.similarityMeasure
			   + "|" +
			   rankerConfig.minSupport
			   + "|" +
			   rankerConfig.minConfidence
			   + "|" +
			   rankerConfig.decay
			   + "|" +
			   minEdgeCount
			   + "|";
	}
}
